
dofile(ModPath .. "hooks/cj.lua")
if not RunMod then return end

log("COP JOBS - SUCCESSFULLY HOOKED INTERACTIONTWEAKDATA")

Hooks:PostHook(InteractionTweakData, "init", "interaction_init_after", function(self, tweak_data)
	
	self.arrestsuspect = deep_clone(self.intimidate)
	
	self.lance_jammed = {
		icon = "develop",
		text_id = "hud_int_equipment_lance_jammed",
		timer = 5,
		sound_start = "bar_power_box_loop",
		sound_interupt = "bar_power_box_cancel",
		sound_done = "bar_power_box_finished",
		upgrade_timer_multiplier = {
			upgrade = "drill_fix_interaction_speed_multiplier",
			category = "player"
		},
		action_text_id = "hud_action_fixing_lance",
		block_upgrade = true
	}
	
	self.place_harddrive = {
		icon = "equipment_stash_server",
		text_id = "hud_int_place_harddrive",
		action_text_id = "hud_action_place_harddrive",
		equipment_text_id = "hud_equipment_need_harddrive",
		special_equipment = "server",
		equipment_consume = true,
		start_active = false,
		timer = 5
	}
	
end)