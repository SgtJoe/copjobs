
log("COP JOBS - SUCCESSFULLY HOOKED BLACKMARKETGUI")

function BlackMarketGui:equip_player_style_callback(data)
	managers.blackmarket:set_equipped_player_style(data.name)
	managers.blackmarket:release_preloaded_category("player_style")
	self:reload()
end

function BlackMarketGui:populate_player_styles(data)
	for i = 1, #data do
		data[i] = nil
	end
	local sort_data = {}
	local tweak, global_value_tweak = nil
	for i, player_style in ipairs(tweak_data.blackmarket.player_style_list) do
		tweak = tweak_data.blackmarket.player_styles[player_style]
		global_value_tweak = tweak_data.lootdrop.global_values[tweak.global_value]
		if Global.blackmarket_manager.player_styles[player_style] and (not global_value_tweak or not global_value_tweak.hide_unavailable or not not managers.dlc:is_global_value_unlocked(tweak.global_value)) then
			table.insert(sort_data, player_style)
		end
	end
	
	local have_suit_variations = nil
	local mannequin_player_style = data.mannequin_player_style or managers.menu_scene and managers.menu_scene:get_player_style() or "none"
	local default_player_style = managers.blackmarket:get_default_player_style()
	local new_data, allow_preview, allow_customize, player_style, player_style_data, guis_catalog, bundle_folder, customize_alpha = nil
	local equipped_player_style = data.equipped_player_style or managers.blackmarket:equipped_player_style()
	local max_items = self:calc_max_items(#sort_data, data.override_slots)
	
	for i = 1, max_items do
		new_data = {
			comparision_data = nil,
			category = "player_styles",
			slot = i
		}
		player_style = sort_data[i]
		
		if player_style then
			allow_preview = true
			player_style_data = tweak_data.blackmarket.player_styles[player_style]
			guis_catalog = "guis/"
			
			new_data.name = player_style
			new_data.name_localized = managers.localization:text(player_style_data.name_id)
			new_data.global_value = player_style_data.global_value or "normal"
			new_data.unlocked = managers.blackmarket:player_style_unlocked(player_style)
			new_data.equipped = equipped_player_style == player_style
			allow_customize = not data.customize_equipped_only or new_data.equipped
			
			if player_style ~= default_player_style then
				new_data.bitmap_texture = guis_catalog .. "textures/pd2/blackmarket/icons/player_styles/" .. player_style
			else
				new_data.bitmap_texture = guis_catalog .. "textures/pd2/blackmarket/icons/player_styles/none"
			end
			
			local is_dlc_locked = tweak_data.lootdrop.global_values[new_data.global_value] and tweak_data.lootdrop.global_values[new_data.global_value].dlc and not managers.dlc:is_dlc_unlocked(new_data.global_value)
			
			if is_dlc_locked then
				new_data.unlocked = false
				new_data.lock_texture = self:get_lock_icon(new_data, "guis/textures/pd2/lock_dlc")
				new_data.dlc_locked = tweak_data.lootdrop.global_values[new_data.global_value] and tweak_data.lootdrop.global_values[new_data.global_value].unlock_id or "bm_menu_dlc_locked"
			elseif not new_data.unlocked then
				local achievement = player_style_data.locks and player_style_data.locks.achievement
				if achievement and managers.achievment:get_info(achievement) and not managers.achievment:get_info(achievement).awarded then
					local achievement_visual = tweak_data.achievement.visual[achievement]
					new_data.lock_texture = "guis/textures/pd2/lock_achievement"
					new_data.dlc_locked = achievement_visual and achievement_visual.desc_id or "achievement_" .. tostring(achievement) .. "_desc"
				else
					new_data.lock_texture = "guis/textures/pd2/skilltree/padlock"
				end
			end
			
			have_suit_variations = tweak_data.blackmarket:have_suit_variations(player_style)
			if have_suit_variations then
				if allow_customize then
					table.insert(new_data, "trd_customize")
				end
				if allow_customize then
					customize_alpha = 0.8
				else
					customize_alpha = 0.4
				end
				new_data.mini_icons = {}
				table.insert(new_data.mini_icons, {
					texture = "guis/dlcs/trd/textures/pd2/blackmarket/paintbrush_icon",
					top = 5,
					h = 16,
					layer = 1,
					w = 16,
					blend_mode = "add",
					right = 5,
					alpha = customize_alpha
				})
			end
			
			if new_data.unlocked and not new_data.equipped then
				table.insert(new_data, "trd_equip")
			end
			
			if allow_preview and mannequin_player_style ~= player_style then
				table.insert(new_data, "trd_preview")
			end
		else
			new_data.name = "empty"
			new_data.name_localized = ""
			new_data.unlocked = true
			new_data.equipped = false
		end
		
		table.insert(data, new_data)
	end
end

Hooks:PostHook(BlackMarketGuiSlotItem, "init", "cj_removelockfromoutfiticons", function(self, main_panel, data, x, y, w, h)
	
	if data.global_value and data.global_value == "previewonly" then
		
		if self._bitmap then
			self._bitmap:set_color(Color.white)
			self._bitmap:set_blend_mode("normal")
		end
		
		if self._lock_bitmap then
			self._lock_bitmap:set_size(0, 0)
		end
	end
end)
