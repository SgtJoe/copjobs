
dofile(ModPath .. "hooks/cj.lua")
if not RunMod then return end

log("COP JOBS - SUCCESSFULLY HOOKED GROUPAITWEAKDATA")

-- Difficulty Cheat Sheet:
-- NAME			ID	STRING
-- Normal		2	normal
-- Hard			3	hard
-- Very Hard	4	overkill
-- Overkill		5	overkill_145
-- Mayhem		6	easy_wish
-- Death Wish	7	overkill_290
-- Death Sent	8	sm_wish

function GroupAITweakData:_init_unit_categories(difficulty_index)
	
	-- Walk = Basic stuff like climbing fences and jumping through windows
	-- Acrobatic = Crazy stuff like rappelling from roofs
	local access_type_walk_only = {walk = true}
	local access_type_all = {acrobatic = true,walk = true}
	
	if difficulty_index <= 2 then
		self.special_unit_spawn_limits = {
			shield = 2,
			medic = 3,
			taser = 1,
			tank = 1,
			spooc = 0
		}
	elseif difficulty_index == 3 then
		self.special_unit_spawn_limits = {
			shield = 4,
			medic = 3,
			taser = 2,
			tank = 2,
			spooc = 0
		}
	elseif difficulty_index == 4 then
		self.special_unit_spawn_limits = {
			shield = 4,
			medic = 3,
			taser = 2,
			tank = 2,
			spooc = 2
		}
	elseif difficulty_index == 5 then
		self.special_unit_spawn_limits = {
			shield = 4,
			medic = 3,
			taser = 2,
			tank = 2,
			spooc = 2
		}
	elseif difficulty_index == 6 then
		self.special_unit_spawn_limits = {
			shield = 4,
			medic = 3,
			taser = 3,
			tank = 2,
			spooc = 2
		}
	elseif difficulty_index == 7 then
		self.special_unit_spawn_limits = {
			shield = 4,
			medic = 3,
			taser = 3,
			tank = 2,
			spooc = 2
		}
	elseif difficulty_index == 8 then
		self.special_unit_spawn_limits = {
			shield = 4,
			medic = 3,
			taser = 3,
			tank = 3,
			spooc = 2
		}
	else
		self.special_unit_spawn_limits = {
			shield = 8,
			medic = 3,
			taser = 4,
			tank = 2,
			spooc = 2
		}
	end
	
	self.unit_categories = {}
	
	-- Unit Category Cheat Sheet:
	-- CS_swat_MP5			Blue SWAT
	-- CS_swat_R870			Blue SWAT (Shotgun)
	-- CS_heavy_M4			Yellow SWAT
	-- CS_heavy_R870		Yellow SWAT (Shotgun)
	-- CS_shield			Shield
	-- CS_tazer				Taser
	-- FBI_swat_M4			FBI/GenSec SWAT
	-- FBI_swat_R870		FBI/GenSec SWAT (Shotgun)
	-- FBI_heavy_G36		FBI/GenSec MFR
	-- FBI_heavy_R870		FBI/GenSec MFR (Shotgun)
	-- FBI_shield			FBI/GenSec Shield
	-- FBI_tank				Bulldozer
	-- medic_M4				Medic
	-- medic_R870			Medic (Shotgun)
	
	
	-- Normal Difficulty (Street Gang)
	if difficulty_index <= 2 then
		self.unit_categories.CS_swat_MP5 = {
			unit_types = {
				america = {
					Idstring("units/payday2/characters/cj_streetgang_2/cj_streetgang_2"),
					Idstring("units/payday2/characters/cj_streetgang_3/cj_streetgang_3")
				}
			},
			access = access_type_walk_only
		}
		self.unit_categories.CS_heavy_M4 = {
			unit_types = {
				america = {
					Idstring("units/payday2/characters/cj_streetgang_1/cj_streetgang_1"),
					Idstring("units/payday2/characters/cj_streetgang_4/cj_streetgang_4")
				}
			},
			access = access_type_walk_only
		}
		self.unit_categories.CS_swat_R870 = deep_clone(self.unit_categories.CS_swat_MP5)
		self.unit_categories.CS_heavy_R870 = deep_clone(self.unit_categories.CS_heavy_M4)
	end
	
	-- Hard Difficulty (Bikers)
	if difficulty_index == 3 then
		self.unit_categories.CS_swat_MP5 = {
			unit_types = {
				america = {
					Idstring("units/payday2/characters/cj_biker_1/cj_biker_1"),
					Idstring("units/payday2/characters/cj_biker_3/cj_biker_3"),
				}
			},
			access = access_type_walk_only
		}
		self.unit_categories.CS_heavy_M4 = {
			unit_types = {
				america = {
					Idstring("units/payday2/characters/cj_biker_2/cj_biker_2"),
					Idstring("units/payday2/characters/cj_biker_4/cj_biker_4")
				}
			},
			access = access_type_walk_only
		}
		self.unit_categories.CS_swat_R870 = deep_clone(self.unit_categories.CS_swat_MP5)
		self.unit_categories.CS_heavy_R870 = deep_clone(self.unit_categories.CS_heavy_M4)
		self.unit_categories.CS_shield = deep_clone(self.unit_categories.CS_swat_MP5)
		self.unit_categories.CS_tazer = {
			special_type = "taser",
			unit_types = {
				america = {
					Idstring("units/payday2/characters/cj_biker_2_m4/cj_biker_2_m4")
				}
			},
			access = access_type_walk_only
		}
	end
	
	-- Very Hard/Overkill Difficulty (Cartel)
	if difficulty_index == 4 or difficulty_index == 5 then
		self.unit_categories.FBI_swat_M4 = {
			unit_types = {
				america = {
					Idstring("units/payday2/characters/cj_cartel_1/cj_cartel_1"),
					Idstring("units/payday2/characters/cj_cartel_2/cj_cartel_2"),
					Idstring("units/payday2/characters/cj_cartel_4/cj_cartel_4")
				}
			},
			access = access_type_walk_only
		}
		self.unit_categories.FBI_heavy_G36 = {
			unit_types = {
				america = {
					Idstring("units/payday2/characters/cj_cartel_heavy/cj_cartel_heavy")
				}
			},
			access = access_type_all
		}
		self.unit_categories.FBI_swat_R870 = deep_clone(self.unit_categories.FBI_swat_M4)
		self.unit_categories.FBI_heavy_R870 = deep_clone(self.unit_categories.FBI_heavy_G36)
		self.unit_categories.FBI_shield = deep_clone(self.unit_categories.FBI_swat_M4)
		self.unit_categories.FBI_shield.special_type = "shield"
		self.unit_categories.CS_tazer = {
			special_type = "taser",
			unit_types = {
				america = {
					Idstring("units/payday2/characters/cj_cartel_3/cj_cartel_3")
				}
			},
			access = access_type_walk_only
		}
		self.unit_categories.FBI_tank = {
			special_type = "tank",
			unit_types = {
				america = {
					Idstring("units/payday2/characters/cj_cartel_tank/cj_cartel_tank")
				}
			},
			access = access_type_all
		}
		self.unit_categories.medic_M4 = {
			special_type = "medic",
			unit_types = {
				america = {
					Idstring("units/payday2/characters/cj_cartel_medic/cj_cartel_medic")
				}
			},
			access = access_type_all
		}
		self.unit_categories.medic_R870 = deep_clone(self.unit_categories.medic_M4)
	end
	
	-- Mayhem/Death Wish Difficulty (Russian Mafia)
	if difficulty_index == 6 or difficulty_index == 7 then
		self.unit_categories.FBI_swat_M4 = {
			unit_types = {
				america = {
					Idstring("units/payday2/characters/cj_russian_1/cj_russian_1"),
					Idstring("units/payday2/characters/cj_russian_2/cj_russian_2"),
					Idstring("units/payday2/characters/cj_russian_4/cj_russian_4")
				}
			},
			access = access_type_walk_only
		}
		self.unit_categories.FBI_heavy_G36 = {
			unit_types = {
				america = {
					Idstring("units/payday2/characters/cj_russian_heavy/cj_russian_heavy")
				}
			},
			access = access_type_all
		}
		self.unit_categories.FBI_swat_R870 = deep_clone(self.unit_categories.FBI_swat_M4)
		self.unit_categories.FBI_heavy_R870 = deep_clone(self.unit_categories.FBI_heavy_G36)
		self.unit_categories.FBI_shield = deep_clone(self.unit_categories.FBI_swat_M4)
		self.unit_categories.FBI_shield.special_type = "shield"
		self.unit_categories.CS_tazer = {
			special_type = "taser",
			unit_types = {
				america = {
					Idstring("units/payday2/characters/cj_russian_3/cj_russian_3")
				}
			},
			access = access_type_walk_only
		}
		if difficulty_index == 6 then
		self.unit_categories.FBI_tank = {
			special_type = "tank",
			unit_types = {
				america = {
					Idstring("units/payday2/characters/cj_russian_tank_1/cj_russian_tank_1")
				}
			},
			access = access_type_all
		}
		else
		self.unit_categories.FBI_tank = {
			special_type = "tank",
			unit_types = {
				america = {
					Idstring("units/payday2/characters/cj_russian_tank_1/cj_russian_tank_1"),
					Idstring("units/payday2/characters/cj_russian_tank_1/cj_russian_tank_1"),
					Idstring("units/payday2/characters/cj_russian_tank_2/cj_russian_tank_2")
				}
			},
			access = access_type_all
		}
		end
		self.unit_categories.medic_M4 = {
			special_type = "medic",
			unit_types = {
				america = {
					Idstring("units/payday2/characters/cj_russian_medic/cj_russian_medic")
				}
			},
			access = access_type_all
		}
		self.unit_categories.medic_R870 = deep_clone(self.unit_categories.medic_M4)
	end
	
	-- Death Sentence Difficulty (we don't actually have anything for DS yet)
	if difficulty_index >= 8 then
		
	end
	
	-- The following are unused, so we're not bothering with them.
	-- CS_cop_C45_R870
	-- CS_cop_stealth_MP5
	-- FBI_suit_C45_M4
	-- FBI_suit_M4_MP5
	-- FBI_suit_stealth_MP5
	-- CS_heavy_M4_w
	-- FBI_heavy_G36_w
end

Hooks:PostHook(GroupAITweakData, "_init_task_data", "difficultyadjust", function(self, difficulty_index, difficulty)
	
	self.besiege.assault.hostage_hesitation_delay = {0, 0, 0}
	
	if difficulty_index <= 2 then
		self.besiege.assault.force = {2, 3, 3} -- 14, 15, 16 -- 14, 16, 18
		self.besiege.assault.force_pool = {10, 15, 20} -- 40, 45, 50 -- 150, 175, 225
	else
		self.besiege.assault.force = {5, 6, 7} -- 14, 15, 16 -- 14, 16, 18
		self.besiege.assault.force_pool = {20, 25, 30} -- 40, 45, 50 -- 150, 175, 225
	end
	
	self.besiege.assault.force_balance_mul = {1, 2, 3, 4} -- 1, 2, 3, 4
	self.besiege.assault.force_pool_balance_mul = {1, 2, 3, 4} -- 1, 2, 3, 4
	
	self.street = deep_clone(self.besiege)
	self.safehouse = deep_clone(self.besiege)
end)
