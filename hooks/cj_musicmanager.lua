
log("COP JOBS - SUCCESSFULLY HOOKED MUSICMANAGER")

function MusicManager:jukebox_default_tracks()
	local default_options = {
		heistlost = "resultscreen_lose",
		loadout = "loadout_music",
		mainmenu = "menu_music",
		credits = "criminals_ambition",
		heistfinish = "music_loot_drop",
		heistresult = "resultscreen_win",
		
		-- heist_cj_proof_name =	"track_35",
		heist_cj_jewelstore_name =	"track_01",
		heist_cj_fourstores_name =	"track_03",
		heist_cj_apartments_name =	"track_08",
		heist_cj_gallery_name =		"track_20",
		heist_cj_bank_name =		"track_04",
		heist_cj_red_name =			"track_31",
		heist_cj_taxman_name =		"track_43",
		heist_cj_mansion_name =		"track_27"
	}
	return default_options
end

--track_05		The Mark
--track_16		Where's the Van
--track_08		Time Window
--track_10		Sirens In the Distnace
--track_47_gen	The Take 2016
--track_04		Razormind
--track_53		Stone Cold 2017
--track_54		Left In The Cold
--track_01		Black Yellow Moebius
--track_52		Double Cross 2017
--track_63		Code Silver 2018
--track_64_lcv	Operation Black Light
--track_02		Full Force Forward
--track_03		Fuse Box
--track_26		Kicking Ass and Taking Names
--track_27		Backstab
--track_07		Tick Tock
--track_44		Sweat
--track_31		Gun Metal Grey 2015
--track_28		Shoutout
--track_20		The Gauntlet
--track_59		Blastaway
--track_21		Something Wicked This Way Comes
--track_30		Utter Chaos
--track_06		Calling All Units
--track_09		Armed To The Teeth
--track_24		Breach 2015
--track_37		Locke And Load
--track_43		Three Way Deal 2016
--track_42		Home Invasion 2016
--track_51		Bullet Rain