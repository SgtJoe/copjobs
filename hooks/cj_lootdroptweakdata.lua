
log("COP JOBS - SUCCESSFULLY HOOKED LOOTDROPTWEAKDATA")

Hooks:PostHook(LootDropTweakData, "init", "cj_lootdroptweakdata_init", function (self, ...)
	
	self.global_values.notforpolice = {
		name_id = "notforpolice_name",
		desc_id = "notforpolice_desc",
		unlock_id = "notforpolice_unlock",
		color = Color(255, 255, 255, 0) / 255,
		dlc = true,
		chance = 1,
		value_multiplier = 1,
		durability_multiplier = 1,
		drops = false,
		track = true,
		sort_number = 101,
		category = "dlc"
	}
	
	self.global_values.previewonly = {
		name_id = "previewonly_name",
		desc_id = "previewonly_desc",
		unlock_id = "previewonly_unlock",
		color = Color(255, 255, 255, 0) / 255,
		dlc = true,
		chance = 1,
		value_multiplier = 1,
		durability_multiplier = 1,
		drops = false,
		track = true,
		sort_number = 101,
		category = "dlc"
	}
	
	self.global_values.doesnotapplyhere = {
		name_id = "doesnotapplyhere_name",
		desc_id = "doesnotapplyhere_desc",
		unlock_id = "doesnotapplyhere_unlock",
		color = Color(255, 255, 255, 0) / 255,
		dlc = true,
		hide_unavailable = true,
		chance = 1,
		value_multiplier = 1,
		durability_multiplier = 1,
		drops = false,
		track = true,
		sort_number = 101,
		category = "dlc"
	}
	
	self.global_values.comingsoon = {
		name_id = "comingsoon_name",
		desc_id = "comingsoon_desc",
		unlock_id = "comingsoon_unlock",
		color = Color(255, 255, 255, 0) / 255,
		dlc = true,
		chance = 1,
		value_multiplier = 1,
		durability_multiplier = 1,
		drops = false,
		track = true,
		sort_number = 101,
		category = "dlc"
	}
	
end)