
dofile(ModPath .. "hooks/cj.lua")
if not RunMod then return end

log("COP JOBS - SUCCESSFULLY HOOKED TEAMAIMOVEMENT")

local difficulty = Global.game_settings and Global.game_settings.difficulty
local difficulty_index = tweak_data:difficulty_to_index(difficulty)

function TeamAIMovement:add_weapons()

	-- This is what makes AI use the host's weapons instead of the clients
	-- That doesn't matter for us,so we remove the check entirely
	-- if Network:is_server() then
		if difficulty_index <= 2 then
			-- Pistols only on Normal
		elseif difficulty_index == 3 then
			-- Shotguns on Hard
			local weapon = "wpn_fps_shot_r870_npc"
			local _ = weapon and self._unit:inventory():add_unit_by_factory_name(weapon, false, false, nil, "")
		else
			-- Rifles for everything else
			local weapon = "wpn_fps_ass_m4_npc"
			local _ = weapon and self._unit:inventory():add_unit_by_factory_name(weapon, false, false, nil, "")
		end
		
		local sec_weap_name = Idstring("units/payday2/weapons/wpn_npc_c45/wpn_npc_c45")
		local _ = sec_weap_name and self._unit:inventory():add_unit_by_name(sec_weap_name)
	-- else
		-- TeamAIMovement.super.add_weapons(self)
	-- end
end
