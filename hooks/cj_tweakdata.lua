
dofile(ModPath .. "hooks/cj.lua")
if not RunMod then return end

log("COP JOBS - SUCCESSFULLY HOOKED TWEAKDATA")

vox_male_1 = "cj_male_1"
vox_male_2 = "cj_male_2"
vox_male_3 = "cj_male_3"
vox_male_4 = "cj_male_4"
vox_male_b = "cj_male_b"
vox_female = "cj_female"

if tweak_data.criminals.characters then
	
	for _, character_data in ipairs(tweak_data.criminals.characters) do
		
		character_data.static_data.ssuffix = "aa"
		
		if CopJobs:option("CopJobs_UseCopVoices") then
			-- Use our custom voices
			if		character_data.name == "spanish" or
					character_data.name == "jacket" or
					character_data.name == "dragon" or
					character_data.name == "chico" or
					character_data.name == "max" or
					character_data.name == "ecp_male" then
				character_data.static_data.voice = vox_male_b
				
			elseif	character_data.name == "sydney" or
					character_data.name == "female_1" or
					character_data.name == "bonnie" or
					character_data.name == "joy" or
					character_data.name == "ecp_female" then
				character_data.static_data.voice = vox_female
				
			else
				local vnum = math.random(1,4)
				if vnum == 1 then
					character_data.static_data.voice = vox_male_1
				elseif vnum == 2 then
					character_data.static_data.voice = vox_male_2
				elseif vnum == 3 then
					character_data.static_data.voice = vox_male_3
				elseif vnum == 4 then
					character_data.static_data.voice = vox_male_4
				end
			end
			
		else
			-- Use vanilla voices, except use Chains voice for all black guys
			if		character_data.name == "spanish" or
					character_data.name == "jacket" or
					character_data.name == "dragon" or
					character_data.name == "chico" or
					character_data.name == "max" or
					character_data.name == "ecp_male" then
				character_data.static_data.voice = "rb1"
			end
		end
		
		-- log("CHARNAME: " .. character_data.name)
		-- log("CHARVOIC: " .. character_data.static_data.voice)
	end
	
	
end
