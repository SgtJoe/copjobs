
log("COP JOBS - SUCCESSFULLY HOOKED PLAYERINVENTORYGUI")

function PlayerInventoryGui:open_armor_menu(selected_tab)
	local new_node_data = {}
	local override_slots = {
		3,
		3
	}
	
	table.insert(new_node_data, {
		name = "bm_menu_armors",
		on_create_func_name = "populate_armors",
		category = "armors",
		override_slots = override_slots,
		identifier = BlackMarketGui.identifiers.armor
	})
	table.insert(new_node_data, {
		name = "bm_menu_player_styles",
		on_create_func_name = "populate_player_styles",
		category = "player_styles",
		override_slots = override_slots,
		identifier = BlackMarketGui.identifiers.player_style
	})
	-- table.insert(new_node_data, {
		-- name = "bm_menu_gloves",
		-- on_create_func_name = "populate_gloves",
		-- category = "gloves",
		-- override_slots = override_slots,
		-- identifier = BlackMarketGui.identifiers.glove
	-- })
	
	new_node_data.topic_id = "bm_menu_outfits"
	new_node_data.selected_tab = selected_tab
	new_node_data.skip_blur = true
	new_node_data.use_bgs = true
	new_node_data.panel_grid_w_mul = 0.6
	
	managers.environment_controller:set_dof_distance(10, false)
	managers.menu_scene:remove_item()
	managers.menu:open_node("blackmarket_outfit_node", {
		new_node_data
	})
end

function PlayerInventoryGui:create_deployable_box(box)
	managers.blackmarket:CJUpdatePlayerEquipment()
end
