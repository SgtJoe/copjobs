--[[
log("COP JOBS - SUCCESSFULLY HOOKED BLACKMARKETTWEAKDATA")

function BlackMarketTweakData:_init_deployables(tweak_data)
	
	self.deployables = {
		doctor_bag_quantity = {name_id = "bm_equipment_doctor_bag"},
		
		first_aid_kit_quantity_increase_2 = {name_id = "bm_equipment_first_aid_kit"}
	}
	
	self:_add_desc_from_name_macro(self.deployables)
end
]]