
dofile(ModPath .. "hooks/cj.lua")
if not RunMod then return end

log("COP JOBS - SUCCESSFULLY HOOKED SENTRYGUNMOVEMENT")

function SentryGunMovement:_upd_hacking(t, dt)
	
	if not self._tweak.ECM_HACKABLE then
		return
	end
	
	local is_hacking_active = false
	
	if self._state == "active" then
		is_hacking_active = true
	end
	
	-- if Network:is_server() then
		-- is_hacking_active = managers.groupai:state():is_ecm_jammer_active("camera")
	-- elseif self._team.id == "hacked_turret" then
		-- is_hacking_active = true
	-- end

	if self._is_hacked then
		if not is_hacking_active then
			self._is_hacked = nil

			if Network:is_server() then
				local original_team = self._original_team
				self._original_team = nil

				self:set_team(original_team)
			end

			if self._hacked_stop_snd_event then
				self._sound_source:post_event(self._hacked_stop_snd_event)
			end

			if Network:is_server() then
				self._unit:brain():on_hacked_end()
			end
		end
	elseif is_hacking_active then
		self._is_hacked = true

		if Network:is_server() then
			local original_team = self._team

			self:set_team(managers.groupai:state():team_data("hacked_turret"))

			self._original_team = original_team
		end

		if self._hacked_start_snd_event then
			self._sound_source:post_event(self._hacked_start_snd_event)
		end

		if Network:is_server() then
			self._unit:brain():on_hacked_start()
		end
	end
end