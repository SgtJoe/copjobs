
dofile(ModPath .. "hooks/cj.lua")

log("COP JOBS - SUCCESSFULLY HOOKED MENUMANAGER")

Hooks:PreHook(MenuCrimeNetContactInfoInitiator, "modify_node", "CJ_AllowImages", function(self, original_node, data)
	self.ALLOW_IMAGES = true
end)

Hooks:Add("MenuManagerInitialize", "CopJobs_menu", function ()
	function MenuCallbackHandler:CopJobs_UseCopVoices(item)
		CopJobs:set_option("CopJobs_UseCopVoices", item:value() == "on")
	end
	
	function MenuCallbackHandler:CopJobs_back()
		CopJobs:save()
	end
	
	MenuHelper:LoadFromJsonFile(CopJobs.mod_options_path, CopJobs, CopJobs.options)
end)
