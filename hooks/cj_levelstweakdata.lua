
dofile(ModPath .. "hooks/cj.lua")

log("COP JOBS - SUCCESSFULLY HOOKED LEVELSTWEAKDATA")

local difficulty = Global.game_settings and Global.game_settings.difficulty

local AlreadyTweaked = false

-- We have to hijack this function because Init runs too early
function LevelsTweakData:get_ai_group_type()
	
	if RunMod and not AlreadyTweaked then
		-- log ("COP JOBS - TWEAKING OUT")
		AlreadyTweaked = true
		
		local forced_primary = "p"
		local forced_secondary = "s"
		local forced_melee = "m"
		local forced_armor = "a"
		local forced_outfit = "o"
		local forced_throwable = "none"
		
		-- REMEMBER: This value is not automatically synced by elements!
		-- If it is updated here, it also need to be MANUALLY CHANGED IN ALL MAPS!
		local wavecount_loot = 3
		local wavecount_noloot = 5
		
		-- Normal = Beat Cop Gear
		if difficulty == "normal" then
			forced_primary = "wpn_fps_pis_x_g17"
			forced_secondary = "wpn_fps_pis_g17"
			forced_melee = "oldbaton"
			forced_armor = "level_1"
			forced_outfit = "cj_beatcop"
			
		-- Hard = Tactical Cop Gear
		elseif difficulty == "hard" then
			forced_primary = "wpn_fps_shot_r870"
			forced_secondary = "wpn_fps_pis_g17"
			forced_melee = "oldbaton"
			forced_armor = "level_2"
			forced_outfit = "cj_tacticalcop"
			
		-- Very Hard/Overkill = SWAT Gear
		elseif difficulty == "overkill" or difficulty == "overkill_145" then
			forced_primary = "wpn_fps_ass_m4"
			forced_secondary = "wpn_fps_pis_g17"
			forced_melee = "kabartanto"
			forced_armor = "level_4"
			forced_outfit = "cj_swat"
			forced_throwable = "concussion"
			
		-- Everything else = Heavy SWAT Gear
		else
			forced_primary = "wpn_fps_ass_m4"
			forced_secondary = "wpn_fps_pis_g17"
			forced_melee = "kabartanto"
			forced_armor = "level_6"
			forced_outfit = "cj_swat_heavy"
			forced_throwable = "concussion"
		end
		
		-- self.cj_proof.ignore_statistics = true
		-- self.cj_proof.disable_mutators = true
		-- self.cj_proof.wave_count = 3
		-- self.cj_proof.hud = {no_hostages = true}
		-- self.cj_proof.player_style = forced_outfit
		-- self.cj_proof.force_equipment = {
			-- throwable = forced_throwable,
			-- melee = forced_melee,
			-- secondary = forced_secondary,
			-- armor = forced_armor,
			-- deployable = "none",
			-- primary = forced_primary
		-- }
		
		self.cj_jewelstore.ignore_statistics = true
		self.cj_jewelstore.disable_mutators = true
		self.cj_jewelstore.wave_count = wavecount_loot
		self.cj_jewelstore.player_style = forced_outfit
		-- self.cj_jewelstore.force_equipment = {
			-- throwable = forced_throwable,
			-- melee = forced_melee,
			-- secondary = forced_secondary,
			-- armor = forced_armor,
			-- deployable = "none",
			-- primary = forced_primary
		-- }
		
		self.cj_fourstores.ignore_statistics = true
		self.cj_fourstores.disable_mutators = true
		self.cj_fourstores.wave_count = wavecount_noloot
		self.cj_fourstores.player_style = forced_outfit
		-- self.cj_fourstores.force_equipment = {
			-- throwable = forced_throwable,
			-- melee = forced_melee,
			-- secondary = forced_secondary,
			-- armor = forced_armor,
			-- deployable = "none",
			-- primary = forced_primary
		-- }
		
		self.cj_apartments.ignore_statistics = true
		self.cj_apartments.disable_mutators = true
		self.cj_apartments.player_style = forced_outfit
		-- self.cj_apartments.force_equipment = {
			-- throwable = forced_throwable,
			-- melee = forced_melee,
			-- secondary = forced_secondary,
			-- armor = forced_armor,
			-- deployable = "none",
			-- primary = forced_primary
		-- }
		
		self.cj_gallery.ignore_statistics = true
		self.cj_gallery.disable_mutators = true
		self.cj_gallery.wave_count = wavecount_loot
		self.cj_gallery.player_style = forced_outfit
		-- self.cj_gallery.force_equipment = {
			-- throwable = forced_throwable,
			-- melee = forced_melee,
			-- secondary = forced_secondary,
			-- armor = forced_armor,
			-- deployable = "none",
			-- primary = forced_primary
		-- }
		
		self.cj_bank.ignore_statistics = true
		self.cj_bank.disable_mutators = true
		self.cj_bank.wave_count = wavecount_loot
		self.cj_bank.player_style = forced_outfit
		-- self.cj_bank.force_equipment = {
			-- throwable = forced_throwable,
			-- melee = forced_melee,
			-- secondary = forced_secondary,
			-- armor = forced_armor,
			-- deployable = "none",
			-- primary = forced_primary
		-- }
		
		self.cj_red.ignore_statistics = true
		self.cj_red.disable_mutators = true
		self.cj_red.player_style = forced_outfit
		
		self.cj_taxman.ignore_statistics = true
		self.cj_taxman.disable_mutators = true
		self.cj_taxman.player_style = forced_outfit
		
		self.cj_mansion.ignore_statistics = true
		self.cj_mansion.disable_mutators = true
		self.cj_mansion.player_style = forced_outfit
		
	else
		-- log ("COP JOBS - NOT TWEAKING")
	end
	
	
	local level_data = Global.level_data and Global.level_data.level_id and self[Global.level_data.level_id]
	if level_data then
		local ai_group_type = level_data.ai_group_type
		if ai_group_type then
			return ai_group_type
		end
	end
	return self.ai_groups.default
end
