--[[
log("COP JOBS - SUCCESSFULLY HOOKED EQUIPMENTSTWEAKDATA")

Hooks:PostHook(EquipmentsTweakData, "init", "cj_equipmentstweakdata_init", function (self)
	
	self.doctor_bag_quantity = {
		deploy_time = 2,
		dummy_unit = "units/payday2/equipment/gen_equipment_medicbag/gen_equipment_medicbag_dummy_unit",
		use_function_name = "use_doctor_bag",
		text_id = "debug_doctor_bag",
		visual_object = "g_medicbag",
		icon = "equipment_doctor_bag",
		description_id = "des_doctor_bag",
		quantity = {
			1
		},
		upgrade_deploy_time_multiplier = {
			upgrade = "deploy_time_multiplier",
			category = "first_aid_kit"
		}
	}
	
	self.first_aid_kit_quantity_increase_2 = {
		deploy_time = 1,
		dummy_unit = "units/pd2_dlc_old_hoxton/equipment/gen_equipment_first_aid_kit/gen_equipment_first_aid_kit_dummy",
		use_function_name = "use_first_aid_kit",
		text_id = "debug_equipment_first_aid_kit",
		visual_object = "g_firstaidbag",
		icon = "equipment_first_aid_kit",
		description_id = "des_first_aid_kit",
		quantity = {
			4
		},
		upgrade_deploy_time_multiplier = {
			upgrade = "deploy_time_multiplier",
			category = "first_aid_kit"
		}
	}
	
end)
]]