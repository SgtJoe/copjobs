
dofile(ModPath .. "hooks/cj.lua")
if not RunMod then return end

log("COP JOBS - SUCCESSFULLY HOOKED MISSIONBRIEFINGGUI")

Hooks:PostHook(NewLoadoutItem, "init", "newloadoutitem_init", function(self, panel, columns, rows, x, y, params)
	
	if not params then return end
	
	if x == 3 and y == 2 then
		local lock = self._item_panel:bitmap({
			texture = "guis/textures/pd2/skilltree/padlock",
			name = "lock",
			h = 32,
			w = 32,
			layer = 2,
			color = tweak_data.screen_colors.text
		})
		
		lock:set_center(self._item_panel:center_x(), self._item_panel:center_y())
		
		if self._item_image then
			self._item_image:set_color(Color.black)
		elseif self._item_image1 and self._item_image2 then
			self._item_image1:set_color(Color.black)
			self._item_image2:set_color(Color.black)
		end
	end
	
	managers.blackmarket:CJUpdatePlayerEquipment()
end)

function NewLoadoutTab:open_node(node)
	self._my_menu_component_data.changing_loadout = nil
	self._my_menu_component_data.current_slot = nil
	
	if node == 1 then
		self._my_menu_component_data.changing_loadout = "primary"
		self._my_menu_component_data.current_slot = managers.blackmarket:equipped_weapon_slot("primaries")
		managers.menu_component:post_event("menu_enter")
		managers.menu:open_node("loadout", {
			self:create_primaries_loadout()
		})
	elseif node == 2 then
		self._my_menu_component_data.changing_loadout = "secondary"
		self._my_menu_component_data.current_slot = managers.blackmarket:equipped_weapon_slot("secondaries")
		managers.menu_component:post_event("menu_enter")
		managers.menu:open_node("loadout", {
			self:create_secondaries_loadout()
		})
	elseif node == 3 then
		managers.menu_component:post_event("menu_enter")
		managers.menu:open_node("loadout", {
			self:create_melee_weapon_loadout()
		})
	elseif node == 4 then
		managers.menu_component:post_event("menu_enter")
		managers.menu:open_node("loadout", {
			self:create_grenade_loadout()
		})
	elseif node == 5 then
		managers.menu_component:post_event("menu_enter")
		managers.menu:open_node("loadout", {
			self:create_armor_loadout()
		})
	-- elseif node == 6 then
		-- managers.menu_component:post_event("menu_enter")
		-- managers.menu:open_node("loadout", {
			-- self:create_deployable_loadout()
		-- })
	end

	managers.menu_component:on_ready_pressed_mission_briefing_gui(false)
end