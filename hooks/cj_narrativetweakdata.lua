
log("COP JOBS - SUCCESSFULLY HOOKED NARRATIVETWEAKDATA")

Hooks:PostHook(NarrativeTweakData, "init", "CJ_NarrativeTweakData_init", function(self)
	
	self.contacts.cj_wpd = {
		name_id = "cj_contact_wpd",
		description_id = "cj_contact_wpd_desc",
		package = "packages/contact_interupt",
		assets_gui = Idstring("guis/mission_briefing/preload_contact_interupt")
	}
	
	self.contacts.cj_nypd = {
		name_id = "cj_contact_nypd",
		description_id = "cj_contact_nypd_desc",
		package = "packages/contact_interupt",
		assets_gui = Idstring("guis/mission_briefing/preload_contact_interupt")
	}
	
	self.contacts.cj_fbi = {
		name_id = "cj_contact_fbi",
		description_id = "cj_contact_fbi_desc",
		package = "packages/contact_interupt",
		assets_gui = Idstring("guis/mission_briefing/preload_contact_interupt")
	}
	
	-- By nulling this out, we hide all vanilla heists, and any custom heists won't cause a crash.
	-- Each map's main.xml does the rest of the job.
	self._jobs_index = {}
	
	
	-- These tidbits are needed for the default music to work
--	self.jobs.cj_proof		= {name_id = "heist_cj_proof_name"}
	self.jobs.cj_jewelstore = {name_id = "heist_cj_jewelstore_name"}
	self.jobs.cj_fourstores = {name_id = "heist_cj_fourstores_name"}
	self.jobs.cj_apartments = {name_id = "heist_cj_apartments_name"}
	self.jobs.cj_gallery	= {name_id = "heist_cj_gallery_name"}
	self.jobs.cj_bank		= {name_id = "heist_cj_bank_name"}
	self.jobs.cj_red		= {name_id = "heist_cj_red_name"}
	self.jobs.cj_taxman		= {name_id = "heist_cj_taxman_name"}
	self.jobs.cj_mansion	= {name_id = "heist_cj_mansion_name"}
	
end)
