
dofile(ModPath .. "hooks/cj.lua")
if not RunMod then return end

log("COP JOBS - SUCCESSFULLY HOOKED HUSKCOPBRAIN")

function HuskCopBrain:on_intimidated(amount, aggressor_unit)
	
	-- log("HUSKING FOR "..amount.." by "..tostring(aggressor_unit))
	
	-- See cj_interactionext.lua for context
	if amount ~= 1337 and amount ~= 6969 then
		amount = math.clamp(math.ceil(amount * 10), 0, 10)
	end
	
	
	self._unit:network():send_to_host("long_dis_interaction", amount, aggressor_unit, false)
	
	return self._interaction_voice
end
