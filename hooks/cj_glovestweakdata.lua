
log("COP JOBS - SUCCESSFULLY HOOKED GLOVESTWEAKDATA")

Hooks:PostHook(BlackMarketTweakData, "_init_gloves", "CJ_PlayerStyleTweakData_init_gloves", function(self, tweak_data)
	
	-- for g_id, g_data in pairs(self.gloves) do
		
		-- if self.gloves[g_id].global_value ~= "inf" then
			-- self.gloves[g_id].global_value = "doesnotapplyhere"
			-- self.gloves[g_id].dlc = "doesnotapplyhere"
		-- end
	-- end
	
	self.gloves.default = {
		name_id = "bm_gloves_default",
		desc_id = "bm_gloves_default_desc",
		texture_bundle_folder = "hnd",
		unit = "units/pd2_dlc_hnd/characters/hnd_glv_heistwrinkled/hnd_glv_heistwrinkled",
		unlocked = true
	}
	
	self.glove_adapter.player_style_exclude_list = {
		"none",
		"nypd",
		"lapd",
		"fbi",
		"gensec",
		"murkywater",
		"cj_beatcop",
		"cj_tacticalcop",
		"cj_swat",
		"cj_swat_heavy",
		"cj_bulldozer",
		"cj_medic",
		"cj_cloaker",
		"cj_medicdozer"
	}
end)