
dofile(ModPath .. "hooks/cj.lua")

log("COP JOBS - SUCCESSFULLY HOOKED PLAYERSTYLETWEAKDATA")

function BlackMarketTweakData:init_cj_style_vars()
	
	body_replacement_all = {
		head = true,
		armor = true,
		body = true,
		hands = true,
		arms = true,
		vest = true
	}
	
	----------------------------------------------------------------------------------------------------------------
	-- First Person meshes
	----------------------------------------------------------------------------------------------------------------
	WPD_1P_short =		"units/cj_wpd/fps/cj_beatcop_fps_short"
	WPD_1P_short_b =	"units/cj_wpd/fps/cj_beatcop_fps_short_black"
	WPD_1P_long =		"units/cj_wpd/fps/cj_beatcop_fps_long"
	WPD_1P_long_b =		"units/cj_wpd/fps/cj_beatcop_fps_long_black"
	WPD_1P_coat =		"units/cj_wpd/fps/cj_beatcop_fps_coat"
	WPD_1P_coat_b =		"units/cj_wpd/fps/cj_beatcop_fps_coat_black"
	WPD_1P_gloves =		"units/cj_wpd/fps/cj_beatcop_fps_gloves"
	WPD_1P_gloves_b =	"units/cj_wpd/fps/cj_beatcop_fps_gloves_black"
	WPD_1P_gloves_l =	"units/cj_wpd/fps/cj_beatcop_fps_gloves_long"
	WPD_1P_swat =		"units/cj_wpd/fps/cj_swat_fps"
	----------------------------------------------------------------------------------------------------------------
	FBI_1P_short =		"units/cj_fbi/fps/cj_fbi_fps_short"
	FBI_1P_short_b =	"units/cj_fbi/fps/cj_fbi_fps_short_black"
	FBI_1P_long =		"units/cj_fbi/fps/cj_fbi_fps_long"
	FBI_1P_long_b =		"units/cj_fbi/fps/cj_fbi_fps_long_black"
	FBI_1P_coat =		"units/cj_fbi/fps/cj_fbi_fps_coat"
	FBI_1P_coat_b =		"units/cj_fbi/fps/cj_fbi_fps_coat_black"
	FBI_1P_gloves =		"units/cj_fbi/fps/cj_fbi_fps_gloves"
	FBI_1P_gloves_b =	"units/cj_fbi/fps/cj_fbi_fps_gloves_black"
	FBI_1P_gloves_l =	"units/cj_fbi/fps/cj_fbi_fps_gloves_long"
	FBI_1P_swat =		"units/cj_fbi/fps/cj_fbi_swat_fps"
	----------------------------------------------------------------------------------------------------------------
	-- Beat Cop (standard police patrol uniform, no armor)
	----------------------------------------------------------------------------------------------------------------
	beatcop_1 =		{unit = WPD_1P_long,	third_unit	= "units/cj_wpd/cj_beatcop_1"}
	beatcop_2 =		{unit = WPD_1P_short,	third_unit	= "units/cj_wpd/cj_beatcop_2"}
	beatcop_3 =		{unit = WPD_1P_long,	third_unit	= "units/cj_wpd/cj_beatcop_3"}
	beatcop_4 =		{unit = WPD_1P_coat,	third_unit	= "units/cj_wpd/cj_beatcop_4"}
	beatcop_5 =		{unit = WPD_1P_short,	third_unit	= "units/cj_wpd/cj_beatcop_5"}
	beatcop_b_1 =	{unit = WPD_1P_long_b,	third_unit	= "units/cj_wpd/cj_beatcop_b_1"}
	beatcop_b_2 =	{unit = WPD_1P_short_b,	third_unit	= "units/cj_wpd/cj_beatcop_b_2"}
	beatcop_b_3 =	{unit = WPD_1P_coat_b,	third_unit	= "units/cj_wpd/cj_beatcop_b_3"}
	beatcop_f_1 =	{unit = WPD_1P_long,	third_unit	= "units/cj_wpd/cj_beatcop_f_1"}
	beatcop_f_2 =	{unit = WPD_1P_long,	third_unit	= "units/cj_wpd/cj_beatcop_f_2"}
	beatcop_f_3 =	{unit = WPD_1P_long,	third_unit	= "units/cj_wpd/cj_beatcop_f_3"}
	beatcop_f_4 =	{unit = WPD_1P_long,	third_unit	= "units/cj_wpd/cj_beatcop_f_4"}
	----------------------------------------------------------------------------------------------------------------
	fbi_1 =		{unit = FBI_1P_long,	third_unit	= "units/cj_fbi/cj_fbi_1"}
	fbi_2 =		{unit = FBI_1P_short,	third_unit	= "units/cj_fbi/cj_fbi_2"}
	fbi_3 =		{unit = FBI_1P_long,	third_unit	= "units/cj_fbi/cj_fbi_3"}
	fbi_4 =		{unit = FBI_1P_coat,	third_unit	= "units/cj_fbi/cj_fbi_4"}
	fbi_5 =		{unit = FBI_1P_short,	third_unit	= "units/cj_fbi/cj_fbi_5"}
	fbi_b_1 =	{unit = FBI_1P_long_b,	third_unit	= "units/cj_fbi/cj_fbi_b_1"}
	fbi_b_2 =	{unit = FBI_1P_short_b,	third_unit	= "units/cj_fbi/cj_fbi_b_2"}
	fbi_b_3 =	{unit = FBI_1P_coat_b,	third_unit	= "units/cj_fbi/cj_fbi_b_3"}
	fbi_f_1 =	{unit = FBI_1P_long,	third_unit	= "units/cj_fbi/cj_fbi_f_1"}
	fbi_f_2 =	{unit = FBI_1P_long,	third_unit	= "units/cj_fbi/cj_fbi_f_2"}
	fbi_f_3 =	{unit = FBI_1P_long,	third_unit	= "units/cj_fbi/cj_fbi_f_3"}
	fbi_f_4 =	{unit = FBI_1P_long,	third_unit	= "units/cj_fbi/cj_fbi_f_4"}
	----------------------------------------------------------------------------------------------------------------
	-- Tactical Cop (police uniform with additional body armor)
	----------------------------------------------------------------------------------------------------------------
	tacticalcop_1 =		{unit = WPD_1P_gloves_l,	third_unit	= "units/cj_wpd/cj_tacticalcop_1"}
	tacticalcop_2 =		{unit = WPD_1P_gloves,		third_unit	= "units/cj_wpd/cj_tacticalcop_2"}
	tacticalcop_3 =		{unit = WPD_1P_gloves_l,	third_unit	= "units/cj_wpd/cj_tacticalcop_3"}
	tacticalcop_4 =		{unit = WPD_1P_coat,		third_unit	= "units/cj_wpd/cj_beatcop_4"}
	tacticalcop_5 =		{unit = WPD_1P_gloves,		third_unit	= "units/cj_wpd/cj_tacticalcop_5"}
	tacticalcop_b_1 =	{unit = WPD_1P_gloves_b,	third_unit	= "units/cj_wpd/cj_tacticalcop_b_1"}
	tacticalcop_b_2 =	{unit = WPD_1P_gloves_l,	third_unit	= "units/cj_wpd/cj_tacticalcop_b_2"}
	tacticalcop_b_3 =	{unit = WPD_1P_coat_b,		third_unit	= "units/cj_wpd/cj_beatcop_b_3"}
	tacticalcop_f_1 =	{unit = WPD_1P_gloves_l,	third_unit	= "units/cj_wpd/cj_tacticalcop_f_1"}
	tacticalcop_f_2 =	{unit = WPD_1P_gloves_l,	third_unit	= "units/cj_wpd/cj_tacticalcop_f_2"}
	tacticalcop_f_3 =	{unit = WPD_1P_long,		third_unit	= "units/cj_wpd/cj_beatcop_f_3"}
	tacticalcop_f_4 =	{unit = WPD_1P_gloves_l,	third_unit	= "units/cj_wpd/cj_tacticalcop_f_4"}
	----------------------------------------------------------------------------------------------------------------
	tacticalfbi_1 =		{unit = FBI_1P_gloves_l,	third_unit	= "units/cj_fbi/cj_tacticalfbi_1"}
	tacticalfbi_2 =		{unit = FBI_1P_gloves,		third_unit	= "units/cj_fbi/cj_tacticalfbi_2"}
	tacticalfbi_3 =		{unit = FBI_1P_gloves_l,	third_unit	= "units/cj_fbi/cj_tacticalfbi_3"}
	tacticalfbi_4 =		{unit = FBI_1P_coat,		third_unit	= "units/cj_fbi/cj_tacticalfbi_4"}
	tacticalfbi_5 =		{unit = FBI_1P_gloves,		third_unit	= "units/cj_fbi/cj_tacticalfbi_5"}
	tacticalfbi_b_1 =	{unit = FBI_1P_gloves_b,	third_unit	= "units/cj_fbi/cj_tacticalfbi_b_1"}
	tacticalfbi_b_2 =	{unit = FBI_1P_gloves_l,	third_unit	= "units/cj_fbi/cj_tacticalfbi_b_2"}
	tacticalfbi_b_3 =	{unit = FBI_1P_coat_b,		third_unit	= "units/cj_fbi/cj_tacticalfbi_b_3"}
	tacticalfbi_f_1 =	{unit = FBI_1P_gloves_l,	third_unit	= "units/cj_fbi/cj_tacticalfbi_f_1"}
	tacticalfbi_f_2 =	{unit = FBI_1P_gloves_l,	third_unit	= "units/cj_fbi/cj_tacticalfbi_f_2"}
	tacticalfbi_f_3 =	{unit = FBI_1P_long,		third_unit	= "units/cj_fbi/cj_tacticalfbi_f_3"}
	tacticalfbi_f_4 =	{unit = FBI_1P_gloves_l,	third_unit	= "units/cj_fbi/cj_tacticalfbi_f_4"}
	----------------------------------------------------------------------------------------------------------------
	-- SWAT (blue variant, moderate armor)
	----------------------------------------------------------------------------------------------------------------
	swat =   {unit = WPD_1P_swat, third_unit = "units/cj_wpd/cj_swat"}
	swat_b = {unit = WPD_1P_swat, third_unit = "units/cj_wpd/cj_swat_black"}
	swat_f = {unit = WPD_1P_swat, third_unit = "units/cj_wpd/cj_swat_female"}
	----------------------------------------------------------------------------------------------------------------
	fbi_swat =   {unit = FBI_1P_swat, third_unit = "units/cj_fbi/cj_fbi_swat"}
	fbi_swat_b = {unit = FBI_1P_swat, third_unit = "units/cj_fbi/cj_fbi_swat_black"}
	fbi_swat_f = {unit = FBI_1P_swat, third_unit = "units/cj_fbi/cj_fbi_swat_female"}
	----------------------------------------------------------------------------------------------------------------
	-- Heavy SWAT (yellow variant, heavy armor)
	----------------------------------------------------------------------------------------------------------------
	swat_heavy =   {unit = WPD_1P_swat, third_unit = "units/cj_wpd/cj_heavyswat"}
	swat_heavy_b = {unit = WPD_1P_swat, third_unit = "units/cj_wpd/cj_heavyswat"}
	swat_heavy_f = {unit = WPD_1P_swat, third_unit = "units/cj_wpd/cj_heavyswat_female"}
	----------------------------------------------------------------------------------------------------------------
	fbi_swat_heavy =   {unit = FBI_1P_swat, third_unit = "units/cj_fbi/cj_fbi_heavyswat"}
	fbi_swat_heavy_b = {unit = FBI_1P_swat, third_unit = "units/cj_fbi/cj_fbi_heavyswat_black"}
	fbi_swat_heavy_f = {unit = FBI_1P_swat, third_unit = "units/cj_fbi/cj_fbi_heavyswat_female"}
	----------------------------------------------------------------------------------------------------------------
	-- Special Units
	----------------------------------------------------------------------------------------------------------------
	bulldozer = {unit = "units/cj_specials/cj_bulldozer_fps", third_unit = "units/cj_specials/cj_bulldozer"}
	
	medic =		{unit = "units/cj_specials/cj_medic_fps", third_unit = "units/cj_specials/cj_medic"}
	medic_b =	{unit = "units/cj_specials/cj_medic_fps", third_unit = "units/cj_specials/cj_medic_black"}
	
	cloaker =	{unit = "units/cj_specials/cj_cloaker_1_fps",		third_unit = "units/cj_specials/cj_cloaker_1"}
	cloaker_b =	{unit = "units/cj_specials/cj_cloaker_1_fps_black", third_unit = "units/cj_specials/cj_cloaker_1_black"}
	cloaker_f =	{unit = "units/cj_specials/cj_cloaker_1_fps",		third_unit = "units/cj_specials/cj_cloaker_1_female"}
end

Hooks:PostHook(BlackMarketTweakData, "_init_player_styles", "CJ_PlayerStyleTweakData__init_player_styles", function(self)
	
	self:init_cj_style_vars()
	
	-- for style_id, style_data in pairs(self.player_styles) do
		
		-- if self.player_styles[style_id].global_value ~= "inf" then
			-- self.player_styles[style_id].global_value = "doesnotapplyhere"
			-- self.player_styles[style_id].dlc = "doesnotapplyhere"
		-- end
	-- end
	
	if RunMod then
		self:AddCJStyles()
	else
		self:AddCJMenuStyles()
	end
end)

function BlackMarketTweakData:AddCJStyles()
	
	log("ADDING OUR CUSTOM STYLES")
	
	-- Character Names Cheat Sheet:
	-- hoxton		Houston
	-- old_hoxton	Hoxton
	-- jowi			Wick
	-- dragan		Dragan
	-- dragon		Jiro
	-- chico		Scarface
	-- myh			Duke
	-- wild			Rust
	-- max			Sangres
	-- female_1		Clover
	-- ecp_male		Ethan
	-- ecp_female	Hila
	
---------------------------------------------------------------- WASHINGTON POLICE DEPARTMENT ----------------------------------------------------------------
	
	self.player_styles.cj_beatcop = {
		body_replacement = body_replacement_all,
		third_body_replacement = body_replacement_all,
		characters = {
			dallas =	beatcop_1,
			wolf =		beatcop_4,
			chains =	beatcop_b_1,
			hoxton =	beatcop_2,
			jowi =		beatcop_1,
			old_hoxton=	beatcop_2,
			dragan =	beatcop_4,
			jacket =	beatcop_b_2,
			sokol =		beatcop_4,
			dragon =	beatcop_b_2,
			bodhi =		beatcop_3,
			jimmy =		beatcop_3,
			chico =		beatcop_b_3,
			myh =		beatcop_5,
			wild =		beatcop_5,
			max =		beatcop_b_1,
			ecp_male =	beatcop_b_3,
			
			sydney =	beatcop_f_1,
			female_1 =	beatcop_f_2,
			bonnie =	beatcop_f_3,
			joy =		beatcop_f_4,
			ecp_female=	beatcop_f_4
		}
	}
	
	self.player_styles.cj_tacticalcop = {
		body_replacement = body_replacement_all,
		third_body_replacement = body_replacement_all,
		characters = {
			dallas =	tacticalcop_1,
			wolf =		tacticalcop_4,
			chains =	tacticalcop_b_1,
			hoxton =	tacticalcop_2,
			jowi =		tacticalcop_1,
			old_hoxton=	tacticalcop_2,
			dragan =	tacticalcop_4,
			jacket =	tacticalcop_b_2,
			sokol =		tacticalcop_4,
			dragon =	tacticalcop_b_2,
			bodhi =		tacticalcop_3,
			jimmy =		tacticalcop_3,
			chico =		tacticalcop_b_3,
			myh =		tacticalcop_5,
			wild =		tacticalcop_5,
			max =		tacticalcop_b_1,
			ecp_male =	tacticalcop_b_3,
			
			sydney =	tacticalcop_f_1,
			female_1 =	tacticalcop_f_2,
			bonnie =	tacticalcop_f_3,
			joy =		tacticalcop_f_4,
			ecp_female=	tacticalcop_f_4
		}
	}
	
	self.player_styles.cj_swat = {
		body_replacement = body_replacement_all,
		third_body_replacement = body_replacement_all,
		characters = {
			dallas =	swat,
			wolf =		swat,
			chains =	swat_b,
			hoxton =	swat,
			jowi =		swat,
			old_hoxton=	swat,
			dragan =	swat,
			jacket =	swat_b,
			sokol =		swat,
			dragon =	swat_b,
			bodhi =		swat,
			jimmy =		swat,
			chico =		swat_b,
			myh =		swat,
			wild =		swat,
			max =		swat_b,
			ecp_male =	swat_b,
			
			sydney =	swat_f,
			female_1 =	swat_f,
			bonnie =	swat_f,
			joy =		swat_f,
			ecp_female=	swat_f
		}
	}
	
	self.player_styles.cj_swat_heavy = {
		body_replacement = body_replacement_all,
		third_body_replacement = body_replacement_all,
		characters = {
			dallas =	swat_heavy,
			wolf =		swat_heavy,
			chains =	swat_heavy_b,
			hoxton =	swat_heavy,
			jowi =		swat_heavy,
			old_hoxton=	swat_heavy,
			dragan =	swat_heavy,
			jacket =	swat_heavy_b,
			sokol =		swat_heavy,
			dragon =	swat_heavy_b,
			bodhi =		swat_heavy,
			jimmy =		swat_heavy,
			chico =		swat_heavy_b,
			myh =		swat_heavy,
			wild =		swat_heavy,
			max =		swat_heavy_b,
			ecp_male =	swat_heavy_b,
			
			sydney =	swat_heavy_f,
			female_1 =	swat_heavy_f,
			bonnie =	swat_heavy_f,
			joy =		swat_heavy_f,
			ecp_female=	swat_heavy_f
		}
	}
	
	self.player_styles.cj_bulldozer = {
		body_replacement = body_replacement_all,
		third_body_replacement = body_replacement_all,
		material_variations = { default = {
			material =		"units/cj_specials/cj_bulldozer_fps_blue",
			third_material= "units/cj_specials/cj_bulldozer_mat_blue"
		}},
		characters = {
			dallas =	bulldozer,
			wolf =		bulldozer,
			chains =	bulldozer,
			hoxton =	bulldozer,
			jowi =		bulldozer,
			old_hoxton=	bulldozer,
			dragan =	bulldozer,
			jacket =	bulldozer,
			sokol =		bulldozer,
			dragon =	bulldozer,
			bodhi =		bulldozer,
			jimmy =		bulldozer,
			chico =		bulldozer,
			myh =		bulldozer,
			wild =		bulldozer,
			max =		bulldozer,
			ecp_male =	bulldozer,
			
			sydney =	bulldozer,
			female_1 =	bulldozer,
			bonnie =	bulldozer,
			joy =		bulldozer,
			ecp_female=	bulldozer
		}
	}
	
	self.player_styles.cj_medic = {
		body_replacement = body_replacement_all,
		third_body_replacement = body_replacement_all,
		characters = {
			dallas =	medic,
			wolf =		medic,
			chains =	medic_b,
			hoxton =	medic,
			jowi =		medic,
			old_hoxton=	medic,
			dragan =	medic,
			jacket =	medic_b,
			sokol =		medic,
			dragon =	medic_b,
			bodhi =		medic,
			jimmy =		medic,
			chico =		medic_b,
			myh =		medic,
			wild =		medic,
			max =		medic_b,
			ecp_male =	medic_b,
			
			sydney =	medic,
			female_1 =	medic,
			bonnie =	medic,
			joy =		medic,
			ecp_female=	medic
		}
	}
	
	self.player_styles.cj_cloaker = {
		body_replacement = body_replacement_all,
		third_body_replacement = body_replacement_all,
		characters = {
			dallas =	cloaker,
			wolf =		cloaker,
			chains =	cloaker_b,
			hoxton =	cloaker,
			jowi =		cloaker,
			old_hoxton=	cloaker,
			dragan =	cloaker,
			jacket =	cloaker_b,
			sokol =		cloaker,
			dragon =	cloaker_b,
			bodhi =		cloaker,
			jimmy =		cloaker,
			chico =		cloaker_b,
			myh =		cloaker,
			wild =		cloaker,
			max =		cloaker_b,
			ecp_male =	cloaker_b,
			
			sydney =	cloaker_f,
			female_1 =	cloaker_f,
			bonnie =	cloaker_f,
			joy =		cloaker_f,
			ecp_female=	cloaker_f
		}
	}
	
	self.player_styles.cj_medicdozer = deep_clone(self.player_styles.cj_bulldozer)
	self.player_styles.cj_medicdozer.material_variations = { default = {
		material =		"units/cj_specials/cj_bulldozer_fps_medic",
		third_material= "units/cj_specials/cj_bulldozer_mat_medic"
	}}
	
------------------------------------------------------------------ NEW YORK POLICE DEPARTMENT ----------------------------------------------------------------
	
	if NYPDjob() then
		
	end
	
---------------------------------------------------------------- FEDERAL BUREAU OF INTERVENTION --------------------------------------------------------------
	
	if FBIjob() then
		self.player_styles.cj_beatcop.characters = {
			dallas =	fbi_1,
			wolf =		fbi_4,
			chains =	fbi_b_1,
			hoxton =	fbi_2,
			jowi =		fbi_1,
			old_hoxton=	fbi_2,
			dragan =	fbi_4,
			jacket =	fbi_b_2,
			sokol =		fbi_4,
			dragon =	fbi_b_2,
			bodhi =		fbi_3,
			jimmy =		fbi_3,
			chico =		fbi_b_3,
			myh =		fbi_5,
			wild =		fbi_5,
			max =		fbi_b_1,
			ecp_male =	fbi_b_3,
			
			sydney =	fbi_f_1,
			female_1 =	fbi_f_2,
			bonnie =	fbi_f_3,
			joy =		fbi_f_4,
			ecp_female=	fbi_f_4
		}
		
		self.player_styles.cj_tacticalcop.characters = {
			dallas =	tacticalfbi_1,
			wolf =		tacticalfbi_4,
			chains =	tacticalfbi_b_1,
			hoxton =	tacticalfbi_2,
			jowi =		tacticalfbi_1,
			old_hoxton=	tacticalfbi_2,
			dragan =	tacticalfbi_4,
			jacket =	tacticalfbi_b_2,
			sokol =		tacticalfbi_4,
			dragon =	tacticalfbi_b_2,
			bodhi =		tacticalfbi_3,
			jimmy =		tacticalfbi_3,
			chico =		tacticalfbi_b_3,
			myh =		tacticalfbi_5,
			wild =		tacticalfbi_5,
			max =		tacticalfbi_b_1,
			ecp_male =	tacticalfbi_b_3,
			
			sydney =	tacticalfbi_f_1,
			female_1 =	tacticalfbi_f_2,
			bonnie =	tacticalfbi_f_3,
			joy =		tacticalfbi_f_4,
			ecp_female=	tacticalfbi_f_4
		}
		
		self.player_styles.cj_swat.characters = {
			dallas =	fbi_swat,
			wolf =		fbi_swat,
			chains =	fbi_swat_b,
			hoxton =	fbi_swat,
			jowi =		fbi_swat,
			old_hoxton=	fbi_swat,
			dragan =	fbi_swat,
			jacket =	fbi_swat_b,
			sokol =		fbi_swat,
			dragon =	fbi_swat_b,
			bodhi =		fbi_swat,
			jimmy =		fbi_swat,
			chico =		fbi_swat_b,
			myh =		fbi_swat,
			wild =		fbi_swat,
			max =		fbi_swat_b,
			ecp_male =	fbi_swat_b,
			
			sydney =	fbi_swat_f,
			female_1 =	fbi_swat_f,
			bonnie =	fbi_swat_f,
			joy =		fbi_swat_f,
			ecp_female=	fbi_swat_f
		}
		
		self.player_styles.cj_swat_heavy.characters = {
			dallas =	fbi_swat_heavy,
			wolf =		fbi_swat_heavy,
			chains =	fbi_swat_heavy_b,
			hoxton =	fbi_swat_heavy,
			jowi =		fbi_swat_heavy,
			old_hoxton=	fbi_swat_heavy,
			dragan =	fbi_swat_heavy,
			jacket =	fbi_swat_heavy_b,
			sokol =		fbi_swat_heavy,
			dragon =	fbi_swat_heavy_b,
			bodhi =		fbi_swat_heavy,
			jimmy =		fbi_swat_heavy,
			chico =		fbi_swat_heavy_b,
			myh =		fbi_swat_heavy,
			wild =		fbi_swat_heavy,
			max =		fbi_swat_heavy_b,
			ecp_male =	fbi_swat_heavy_b,
			
			sydney =	fbi_swat_heavy_f,
			female_1 =	fbi_swat_heavy_f,
			bonnie =	fbi_swat_heavy_f,
			joy =		fbi_swat_heavy_f,
			ecp_female=	fbi_swat_heavy_f
		}
		
		self.player_styles.cj_bulldozer.material_variations = { default = {
			material =		"units/cj_specials/cj_bulldozer_fps_green",
			third_material= "units/cj_specials/cj_bulldozer_mat_green"
		}}
	end
end

function BlackMarketTweakData:AddCJMenuStyles()
	log("ADDING PLACEHOLDER MENU STYLES")
	
	-- Default - WPD
	self.player_styles.none = {
		name_id = "bm_police_wpd",
		desc_id = "bm_police_wpd_desc",
		cj_style = true,
		police_shared = true,
		unlocked = true,
		texture_bundle_folder = nil,
		body_replacement = body_replacement_all,
		third_body_replacement = body_replacement_all,
		mat_overrides = {
			beatcop =	"units/cj_wpd/cj_wpd_atlas",
			tactical =	"units/cj_wpd/cj_wpd_atlas",
			swat =		"units/cj_wpd/cj_wpd_swat_atlas",
			heavy =		"units/cj_wpd/cj_wpd_swat_atlas",
			dozer =		"units/cj_specials/cj_bulldozer_mat_blue",
			medic =		"units/cj_specials/cj_medic_mat",
			cloaker =	"units/cj_specials/cj_cloaker_mat",
			medicdozer= "units/cj_specials/cj_bulldozer_mat_medic"
		},
		characters = {
			dallas =	beatcop_1,
			wolf =		beatcop_4,
			chains =	beatcop_b_1,
			hoxton =	beatcop_2,
			jowi =		beatcop_1,
			old_hoxton=	beatcop_2,
			dragan =	beatcop_4,
			jacket =	beatcop_b_2,
			sokol =		beatcop_4,
			dragon =	beatcop_b_2,
			bodhi =		beatcop_3,
			jimmy =		beatcop_3,
			chico =		beatcop_b_3,
			myh =		beatcop_5,
			wild =		beatcop_5,
			max =		beatcop_b_1,
			ecp_male =	beatcop_b_3,
			
			sydney =	beatcop_f_1,
			female_1 =	beatcop_f_2,
			bonnie =	beatcop_f_3,
			joy =		beatcop_f_4,
			ecp_female=	beatcop_f_4
		}
	}
	self.player_styles.none.characters_beatcop = deep_clone(self.player_styles.none.characters)
	self.player_styles.none.characters_tactical = {
		dallas =	tacticalcop_1,
		wolf =		tacticalcop_4,
		chains =	tacticalcop_b_1,
		hoxton =	tacticalcop_2,
		jowi =		tacticalcop_1,
		old_hoxton=	tacticalcop_2,
		dragan =	tacticalcop_4,
		jacket =	tacticalcop_b_2,
		sokol =		tacticalcop_4,
		dragon =	tacticalcop_b_2,
		bodhi =		tacticalcop_3,
		jimmy =		tacticalcop_3,
		chico =		tacticalcop_b_3,
		myh =		tacticalcop_5,
		wild =		tacticalcop_5,
		max =		tacticalcop_b_1,
		ecp_male =	tacticalcop_b_3,
		
		sydney =	tacticalcop_f_1,
		female_1 =	tacticalcop_f_2,
		bonnie =	tacticalcop_f_3,
		joy =		tacticalcop_f_4,
		ecp_female=	tacticalcop_f_4
	}
	self.player_styles.none.characters_swat = {
		dallas =	swat,
		wolf =		swat,
		chains =	swat_b,
		hoxton =	swat,
		jowi =		swat,
		old_hoxton=	swat,
		dragan =	swat,
		jacket =	swat_b,
		sokol =		swat,
		dragon =	swat_b,
		bodhi =		swat,
		jimmy =		swat,
		chico =		swat_b,
		myh =		swat,
		wild =		swat,
		max =		swat_b,
		ecp_male =	swat_b,
		
		sydney =	swat_f,
		female_1 =	swat_f,
		bonnie =	swat_f,
		joy =		swat_f,
		ecp_female=	swat_f
	}
	self.player_styles.none.characters_heavy = {
		dallas =	swat_heavy,
		wolf =		swat_heavy,
		chains =	swat_heavy_b,
		hoxton =	swat_heavy,
		jowi =		swat_heavy,
		old_hoxton=	swat_heavy,
		dragan =	swat_heavy,
		jacket =	swat_heavy_b,
		sokol =		swat_heavy,
		dragon =	swat_heavy_b,
		bodhi =		swat_heavy,
		jimmy =		swat_heavy,
		chico =		swat_heavy_b,
		myh =		swat_heavy,
		wild =		swat_heavy,
		max =		swat_heavy_b,
		ecp_male =	swat_heavy_b,
		
		sydney =	swat_heavy_f,
		female_1 =	swat_heavy_f,
		bonnie =	swat_heavy_f,
		joy =		swat_heavy_f,
		ecp_female=	swat_heavy_f
	}
	self.player_styles.none.characters_medic = {
		dallas =	medic,
		wolf =		medic,
		chains =	medic_b,
		hoxton =	medic,
		jowi =		medic,
		old_hoxton=	medic,
		dragan =	medic,
		jacket =	medic_b,
		sokol =		medic,
		dragon =	medic_b,
		bodhi =		medic,
		jimmy =		medic,
		chico =		medic_b,
		myh =		medic,
		wild =		medic,
		max =		medic_b,
		ecp_male =	medic_b,
		
		sydney =	medic,
		female_1 =	medic,
		bonnie =	medic,
		joy =		medic,
		ecp_female=	medic
	}
	self.player_styles.none.characters_cloaker = {
		dallas =	cloaker,
		wolf =		cloaker,
		chains =	cloaker_b,
		hoxton =	cloaker,
		jowi =		cloaker,
		old_hoxton=	cloaker,
		dragan =	cloaker,
		jacket =	cloaker_b,
		sokol =		cloaker,
		dragon =	cloaker_b,
		bodhi =		cloaker,
		jimmy =		cloaker,
		chico =		cloaker_b,
		myh =		cloaker,
		wild =		cloaker,
		max =		cloaker_b,
		ecp_male =	cloaker_b,
		
		sydney =	cloaker_f,
		female_1 =	cloaker_f,
		bonnie =	cloaker_f,
		joy =		cloaker_f,
		ecp_female=	cloaker_f
	}
	
	-- NYPD
	self.player_styles.nypd = deep_clone(self.player_styles.none)
	self.player_styles.nypd.name_id = "bm_police_nypd"
	self.player_styles.nypd.desc_id = "bm_police_nypd_desc"
	self.player_styles.nypd.global_value = "doesnotapplyhere"
	self.player_styles.nypd.unlocked = false
	
	-- FBI
	self.player_styles.fbi = {
		name_id = "bm_fbi",
		desc_id = "bm_fbi_desc",
		cj_style = true,
		police_shared = false,
		unlocked = false,
		global_value = "previewonly",
		texture_bundle_folder = nil,
		body_replacement = body_replacement_all,
		third_body_replacement = body_replacement_all,
		mat_overrides = {
			beatcop =	"units/cj_fbi/cj_fbi_atlas",
			tactical =	"units/cj_fbi/cj_fbi_atlas",
			swat =		"units/cj_fbi/cj_fbi_swat_atlas",
			heavy =		"units/cj_fbi/cj_fbi_swat_atlas",
			dozer =		"units/cj_specials/cj_bulldozer_mat_green",
			medic =		"units/cj_specials/cj_medic_mat",
			cloaker =	"units/cj_specials/cj_cloaker_mat",
			medicdozer= "units/cj_specials/cj_bulldozer_mat_medic"
		},
		characters = {
			dallas =	fbi_1,
			wolf =		fbi_4,
			chains =	fbi_b_1,
			hoxton =	fbi_2,
			jowi =		fbi_1,
			old_hoxton=	fbi_2,
			dragan =	fbi_4,
			jacket =	fbi_b_2,
			sokol =		fbi_4,
			dragon =	fbi_b_2,
			bodhi =		fbi_3,
			jimmy =		fbi_3,
			chico =		fbi_b_3,
			myh =		fbi_5,
			wild =		fbi_5,
			max =		fbi_b_1,
			ecp_male =	fbi_b_3,
			
			sydney =	fbi_f_1,
			female_1 =	fbi_f_2,
			bonnie =	fbi_f_3,
			joy =		fbi_f_4,
			ecp_female=	fbi_f_4
		}
	}
	self.player_styles.fbi.characters_beatcop = deep_clone(self.player_styles.fbi.characters)
	self.player_styles.fbi.characters_tactical = {
		dallas =	tacticalfbi_1,
		wolf =		tacticalfbi_4,
		chains =	tacticalfbi_b_1,
		hoxton =	tacticalfbi_2,
		jowi =		tacticalfbi_1,
		old_hoxton=	tacticalfbi_2,
		dragan =	tacticalfbi_4,
		jacket =	tacticalfbi_b_2,
		sokol =		tacticalfbi_4,
		dragon =	tacticalfbi_b_2,
		bodhi =		tacticalfbi_3,
		jimmy =		tacticalfbi_3,
		chico =		tacticalfbi_b_3,
		myh =		tacticalfbi_5,
		wild =		tacticalfbi_5,
		max =		tacticalfbi_b_1,
		ecp_male =	tacticalfbi_b_3,
		
		sydney =	tacticalfbi_f_1,
		female_1 =	tacticalfbi_f_2,
		bonnie =	tacticalfbi_f_3,
		joy =		tacticalfbi_f_4,
		ecp_female=	tacticalfbi_f_4
	}
	self.player_styles.fbi.characters_swat = {
		dallas =	fbi_swat,
		wolf =		fbi_swat,
		chains =	fbi_swat_b,
		hoxton =	fbi_swat,
		jowi =		fbi_swat,
		old_hoxton=	fbi_swat,
		dragan =	fbi_swat,
		jacket =	fbi_swat_b,
		sokol =		fbi_swat,
		dragon =	fbi_swat_b,
		bodhi =		fbi_swat,
		jimmy =		fbi_swat,
		chico =		fbi_swat_b,
		myh =		fbi_swat,
		wild =		fbi_swat,
		max =		fbi_swat_b,
		ecp_male =	fbi_swat_b,
		
		sydney =	fbi_swat_f,
		female_1 =	fbi_swat_f,
		bonnie =	fbi_swat_f,
		joy =		fbi_swat_f,
		ecp_female=	fbi_swat_f
	}
	self.player_styles.fbi.characters_heavy = {
		dallas =	fbi_swat_heavy,
		wolf =		fbi_swat_heavy,
		chains =	fbi_swat_heavy_b,
		hoxton =	fbi_swat_heavy,
		jowi =		fbi_swat_heavy,
		old_hoxton=	fbi_swat_heavy,
		dragan =	fbi_swat_heavy,
		jacket =	fbi_swat_heavy_b,
		sokol =		fbi_swat_heavy,
		dragon =	fbi_swat_heavy_b,
		bodhi =		fbi_swat_heavy,
		jimmy =		fbi_swat_heavy,
		chico =		fbi_swat_heavy_b,
		myh =		fbi_swat_heavy,
		wild =		fbi_swat_heavy,
		max =		fbi_swat_heavy_b,
		ecp_male =	fbi_swat_heavy_b,
		
		sydney =	fbi_swat_heavy_f,
		female_1 =	fbi_swat_heavy_f,
		bonnie =	fbi_swat_heavy_f,
		joy =		fbi_swat_heavy_f,
		ecp_female=	fbi_swat_heavy_f
	}
	self.player_styles.fbi.characters_medic = deep_clone(self.player_styles.none.characters_medic)
	self.player_styles.fbi.characters_cloaker = deep_clone(self.player_styles.none.characters_cloaker)
	
	-- This data doesn't have to be accurate, it just needs to exist, so it's easier to do it like this rather than cluttering the tables with even more copypasta
	for s, _ in pairs(self.player_styles) do
		if self.player_styles[s].cj_style then
			self.player_styles[s].material_variations = {default = {third_material = "units/cj_wpd/cj_wpd_atlas"}}
		end
	end
end

function BlackMarketTweakData:GetCJThirdUnitForMenu(armor_id, mychar, style, suitvar, IgnoreSkills)
	
	if not self.player_styles[style].cj_style then
		log("REVERTING INVALID STYLE "..style)
		style = "none"
		managers.blackmarket:CJ_RevertInvalidStyle()
	end
	
	local return_style = beatcop_1.third_unit
	local armor_level = tonumber(string.sub(armor_id, 7))
	
	-- log("GETTING "..armor_level.." "..mychar.." "..style.." "..suitvar)
	
	-- level_1 = Suit
	-- level_2 = Light Ballistic Vest
	-- level_3 = Ballistic Vest
	-- level_4 = Heavy Ballistic Vest
	-- level_5 = Flak Jacket
	-- level_6 = Combined Tactical Vest
	-- level_7 = Improved Combined Tactical Vest
	
	-- Suit = Beat Cop
	if armor_level <= 1 then
		return_style = tostring(self.player_styles[style].characters_beatcop[mychar].third_unit)
	end
	
	-- Light Vest = Tactical Cop
	if armor_level == 2 then
		return_style = tostring(self.player_styles[style].characters_tactical[mychar].third_unit)
	end
	
	-- Medium Vests = SWAT
	if armor_level == 3 or armor_level == 4 then
		return_style = tostring(self.player_styles[style].characters_swat[mychar].third_unit)
	end
	
	-- Ballistic Vest = Heavy SWAT
	if armor_level == 5 or armor_level == 6 then
		return_style = tostring(self.player_styles[style].characters_heavy[mychar].third_unit)
	end
	
	-- ICTV = Bulldozer
	if armor_level >= 7 then
		return_style = tostring(bulldozer.third_unit)
	end
	
	if not managers.player or IgnoreSkills then return return_style end
	
	-- Inspire = Medic
	if managers.player:has_category_upgrade("cooldown", "long_dis_revive") then
		return_style = tostring(self.player_styles[style].characters_medic[mychar].third_unit)
	end
	
	-- Shockproof = Taser
	-- if managers.player:has_category_upgrade("player", "escape_taser") then
		
	-- end
	
	-- Sneaky Bastard = Cloaker
	if managers.player:has_category_upgrade("player", "detection_risk_add_dodge_chance") then
		return_style = tostring(self.player_styles[style].characters_cloaker[mychar].third_unit)
	end
	
	-- Inspire + ICTV = Medic Bulldozer
	if managers.player:has_category_upgrade("cooldown", "long_dis_revive") and armor_level >= 7 then
		return_style = ""..tostring(bulldozer.third_unit)
	end
	
	if return_style == "nil" then
		log("BAD STYLE!")
		return beatcop_1.third_unit
	end
	
	return return_style
end

function BlackMarketTweakData:GetCJThirdMatForMenu(armor_id, player_style, IgnoreSkills)
	
	if not self.player_styles[player_style].cj_style then
		log("REVERTING INVALID STYLE "..player_style)
		player_style = "none"
		managers.blackmarket:CJ_RevertInvalidStyle()
	end
	
	local return_mat = "units/cj_specials/cj_bulldozer_fps_green"
	local armor_level = tonumber(string.sub(armor_id, 7))
	
	-- log("GETTING MAT "..armor_level.." "..player_style)
	
	if armor_level <= 1 then
		return_mat = tweak_data.blackmarket.player_styles[player_style].mat_overrides["beatcop"]
	end
	
	if armor_level == 2 then
		return_mat = tweak_data.blackmarket.player_styles[player_style].mat_overrides["tactical"]
	end
	
	if armor_level == 3 or armor_level == 4 then
		return_mat = tweak_data.blackmarket.player_styles[player_style].mat_overrides["swat"]
	end
	
	if armor_level == 5 or armor_level == 6 then
		return_mat = tweak_data.blackmarket.player_styles[player_style].mat_overrides["heavy"]
	end
	
	if armor_level >= 7 then
		return_mat = tweak_data.blackmarket.player_styles[player_style].mat_overrides["dozer"]
	end
	
	if not managers.player or IgnoreSkills then return return_mat end
	
	if managers.player:has_category_upgrade("cooldown", "long_dis_revive") then
		return_mat = tweak_data.blackmarket.player_styles[player_style].mat_overrides["medic"]
	end
	
	-- Shockproof = Taser
	-- if managers.player:has_category_upgrade("player", "escape_taser") then
		
	-- end
	
	-- Sneaky Bastard = Cloaker
	if managers.player:has_category_upgrade("player", "detection_risk_add_dodge_chance") then
		return_mat = tweak_data.blackmarket.player_styles[player_style].mat_overrides["cloaker"]
	end
	
	-- Inspire + ICTV = Medic Bulldozer
	if managers.player:has_category_upgrade("cooldown", "long_dis_revive") and armor_level >= 7 then
		return_mat = tweak_data.blackmarket.player_styles[player_style].mat_overrides["medicdozer"]
	end
	
	return return_mat
end
