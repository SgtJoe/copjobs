
if not CopJobs then
	
	log("COP JOBS STARTING UP")
	
	CopJobs = {
		mod_path = ModPath,
		save_path = SavePath .. "CopJobs.txt",
		localization_path = ModPath .. "localization/",
		mod_options_path = ModPath .. "mod_options.txt",
		options = {
			CopJobs_UseCopVoices = false
		}
	}
	
	function CopJobs:save()
		local file = io.open(self.save_path, "w+")
		if file then
			file:write(json.encode(self.options))
			file:close()
		end
	end
	
	function CopJobs:load()
		local file = io.open(self.save_path, "r")
		if file then
			self.options = json.decode(file:read("*a"))
			file:close()
		end
	end
	
	function CopJobs:set_option(name, value)
		self.options[name] = value
	end
	
	function CopJobs:option(name)
		return self.options[name]
	end
	
	CopJobs:load()
end

if not Global.level_data then
	-- log("COULD NOT GET   AAAAAAAAAAAAAAAAA")
	RunMod = false
return end

-- Remember to update:
-- cj_levelstweakdata
-- cj_musicmanager
-- cj_narrativetweakdata
-- localization

all_cj_heists = {
	-- "cj_proof",
	"cj_jewelstore",
	"cj_fourstores",
	"cj_apartments",
	"cj_gallery",
	"cj_bank",
	"cj_red",
	"cj_taxman",
	"cj_mansion"
}

cj_no_sniper_stages = {
	"cj_apartments",
	"cj_gallery"
}

cj_nypd_stages = {
	-- "cj_carchase"
}

cj_fbi_stages = {
	"cj_red",
	"cj_taxman",
	"cj_mansion"
}

cj_fbi_backup_stages = {
	"cj_red",
	"cj_taxman",
	"cj_mansion"
}

cj_turret_stages = {
	"cj_mansion"
}

function NYPDjob()
	return table.contains(cj_nypd_stages, Global.level_data.level_id)
end

function FBIjob()
	return table.contains(cj_fbi_stages, Global.level_data.level_id)
end

function IsValidCopJob()
	return table.contains(all_cj_heists, Global.level_data.level_id)
end

if IsValidCopJob() then
	RunMod = true
else
	log ("COP JOBS - NOT RUNNING ON INVALID MAP")
	RunMod = false
end
