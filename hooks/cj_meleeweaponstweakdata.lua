
log("COP JOBS - SUCCESSFULLY HOOKED MELEEWEAPONSTWEAKDATA")

Hooks:PostHook(BlackMarketTweakData, "_init_melee_weapons", "cj_init_melee_weapons", function(self, tweak_data)
	
	local police_melee = table.list_to_set({
		'weapon',
		'fists',
		'baton',
		'oldbaton',
		'aziz',
		'kabar',
		-- 'x46',
		'taser'
	})
	
	for weapon_id, weapon_data in pairs(self.melee_weapons) do
		if not police_melee[weapon_id] then
			self.melee_weapons[weapon_id].dlc = "notforpolice"
			self.melee_weapons[weapon_id].locks = {dlc = "notforpolice"}
		end
	end
	
end)
