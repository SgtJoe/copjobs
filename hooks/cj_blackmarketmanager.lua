
log("COP JOBS - SUCCESSFULLY HOOKED BLACKMARKETMANAGER")

CurrentlyPreviewing = nil

local Locked_Bool = {true, true}

Hooks:PostHook(BlackMarketManager, "_load_done", "cj_blackmarketmanager_load_done", function(bb, ...)
	if Locked_Bool[1] then
		Locked_Bool[1] = nil
		LockBadContent()
	end
end )

Hooks:PostHook(BlackMarketManager, "_setup", "cj_blackmarketmanager_setup", function(self, ...)
	if Locked_Bool[2] then
		Locked_Bool[2] = nil
		LockBadContent()
	end
end)

function LockBadContent()
	
	for weapon_id, weapon_data in pairs(Global.blackmarket_manager.weapons) do
		local _wd = tweak_data.weapon[weapon_id] or nil
		if _wd and _wd.global_value == 'notforpolice' then
			Global.blackmarket_manager.weapons[weapon_id].unlocked = false
		end
	end
	
	for melee_id, melee_data in pairs(Global.blackmarket_manager.melee_weapons) do
		local _md = tweak_data.blackmarket.melee_weapons[melee_id] or nil
		if _md and _md.dlc == 'notforpolice' then
			Global.blackmarket_manager.melee_weapons[melee_id].unlocked = false
		end
	end
	
	for grenade_id, grenade_data in pairs(Global.blackmarket_manager.grenades) do
		local _gd = tweak_data.blackmarket.projectiles[grenade_id] or nil
		if _gd and _gd.dlc == 'notforpolice' then
			Global.blackmarket_manager.grenades[grenade_id].unlocked = false
			Global.blackmarket_manager.grenades[grenade_id].equipped = false
			Global.blackmarket_manager.grenades[grenade_id].amount = 0
		end
	end
end

function BlackMarketManager:forced_deployable()
	
	local equip = nil
	
	if managers.player then
		
		if managers.player:has_category_upgrade("trip_mine", "quantity") then
			equip = "trip_mine"
		end
		
		if managers.player:has_category_upgrade("ammo_bag", "quantity") then
			equip = "ammo_bag"
		end
		
		if managers.player:has_category_upgrade("first_aid_kit", "quantity") then
			equip = "first_aid_kit"
		end
		
	end
	
	return equip
end

function BlackMarketManager:CJUpdatePlayerEquipment()
	
	local forcedequip = self:forced_deployable()
	
	-- log("FORCING EQUIPMENT "..tostring(forcedequip))
	
	self:equip_deployable({
		target_slot = 1,
		name = forcedequip
	})
	
	if managers.player and managers.player:has_category_upgrade("player", "second_deployable") then
		self:equip_deployable({target_slot = 2})
	end
end

function BlackMarketManager:equip_next_deployable(slot) end
function BlackMarketManager:equip_previous_deployable(slot) end

-- Force the character to reload whenever we change or preview armor/outfits
Hooks:PostHook(BlackMarketManager, "equip_armor", "cj_blackmarketmanager_equip_armor", function(self, armor_id)
	if managers.menu_scene then
		managers.menu_scene:set_character(self:get_preferred_character(), true)
	end
end)

Hooks:PostHook(BlackMarketManager, "set_equipped_player_style", "cj_blackmarketmanager_set_equipped_player_style", function(self, player_style, loading)
	if not loading and managers.menu_scene then
		managers.menu_scene:set_character(self:get_preferred_character(), true)
	end
end)

Hooks:PreHook(BlackMarketManager, "view_player_style", "cj_blackmarketmanager_view_player_style_pre", function(self, player_style, material_variation, done_cb)
	self.CurrentlyPreviewing = player_style
	-- log("PREVIEWEING: "..tostring(self.CurrentlyPreviewing))
end)

Hooks:PostHook(BlackMarketManager, "view_player_style", "cj_blackmarketmanager_view_player_style_post", function(self, player_style, material_variation, done_cb)
	if managers.menu_scene then
		managers.menu_scene:set_character(self:get_preferred_character(), true)
	end
	self.CurrentlyPreviewing = nil
	-- log("STOP PREVIEWEING")
end)

function BlackMarketManager:CJ_RevertInvalidStyle()
	Global.blackmarket_manager.equipped_player_style = "none"
	
	if managers.menu_scene then
		managers.menu_scene:set_character_player_style("none", self:get_suit_variation("none"))
	end
	
	MenuCallbackHandler:_update_outfit_information()
	
	-- if SystemInfo:distribution() == Idstring("STEAM") then
		-- managers.statistics:publish_equipped_to_steam()
	-- end
end