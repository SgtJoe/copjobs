
dofile(ModPath .. "hooks/cj.lua")
if not RunMod then return end

log("COP JOBS - SUCCESSFULLY HOOKED CRIMINALSMANAGER")

function CriminalsManager:update_character_visual_state(character_name, visual_state)
	local character = self:character_by_name(character_name)
	
	if not character or not character.taken or not alive(character.unit) then
		return
	end
	
	if not character.data.ai then
		-- log("CHARDUMP-------------------------------------------------------------------------------------")
		
		-- for id, data in pairs(character) do
			-- log(tostring(id).." = "..tostring(data))
		-- end
		
		-- log("-STATIC_DATA:")
		-- if character.static_data then
			-- for id, data in pairs(character.static_data) do
				-- log("     "..tostring(id).." = "..tostring(data))
			-- end
		-- end
		
		-- log("-VISUAL_STATE:")
		-- if visual_state then
			-- for id, data in pairs(visual_state) do
				-- log("     "..tostring(id).." = "..tostring(data))
			-- end
		-- end
		
		-- log("-DATA:")
		-- if character.data then
			-- for id, data in pairs(character.data) do
				-- log("     "..tostring(id).." = "..tostring(data))
			-- end
		-- end
	-- else
		-- log("not dumping AI")
	end
	
	visual_state = visual_state or {}
	local unit = character.unit
	local is_local_peer = visual_state.is_local_peer or character.visual_state.is_local_peer or false
	local visual_seed = visual_state.visual_seed or character.visual_state.visual_seed or CriminalsManager.get_new_visual_seed()
	local mask_id = visual_state.mask_id or character.visual_state.mask_id
	local armor_id = visual_state.armor_id or character.visual_state.armor_id or "level_1"
	local armor_skin = visual_state.armor_skin or character.visual_state.armor_skin or "none"
	local player_style = self:active_player_style() or managers.blackmarket:get_default_player_style()
	local suit_variation = nil
	local user_player_style = visual_state.player_style or character.visual_state.player_style or managers.blackmarket:get_default_player_style()
	
	if not self:is_active_player_style_locked() and user_player_style ~= managers.blackmarket:get_default_player_style() then
		player_style = user_player_style
		suit_variation = visual_state.suit_variation or character.visual_state.suit_variation or "default"
	end
	
	local myspecial = nil
	
	if managers.player then
		
		-- log("CURRENT STYLE: "..player_style)
		
		-- Local Peer = "us", the player viewing the screen right now
		if is_local_peer then
			
			-- log("STYLING LOCAL PLAYER")
			
			-- Change outfit depending on equipped armor
			local armor_level = tonumber(string.sub(armor_id, 7))
			
			-- log("OUR POWERLEVEL IS: "..armor_level)
			
			-- level_1 = Suit
			-- level_2 = Light Ballistic Vest
			-- level_3 = Ballistic Vest
			-- level_4 = Heavy Ballistic Vest
			-- level_5 = Flak Jacket
			-- level_6 = Combined Tactical Vest
			-- level_7 = Improved Combined Tactical Vest
			
			-- Suit = Beat Cop
			if armor_level <= 1 then
				player_style = "cj_beatcop"
			
			-- Light Vest = Tactical Cop
			elseif armor_level == 2 then
				player_style = "cj_tacticalcop"
				
			-- Medium Vests = SWAT
			elseif armor_level <= 4 then
				player_style = "cj_swat"
			
			-- Ballistic Vest = Heavy SWAT
			elseif armor_level <= 6 then
				player_style = "cj_swat_heavy"
			end
			
			-- ICTV = Bulldozer
			if armor_level >= 7 then
				player_style = "cj_bulldozer"
			end
			
			
			
			-- Inspire Aced = Medic
			if managers.player:has_category_upgrade("cooldown", "long_dis_revive") then
				player_style = "cj_medic"
				myspecial = "cj_medic"
			end
			
			-- Shockproof Aced = Taser
			-- if managers.player:has_category_upgrade("player", "escape_taser") then
				-- player_style = "cj_taser"
				-- myspecial = "cj_taser"
			-- end
			
			-- Sneaky Bastard Aced = Cloaker
			if managers.player:has_category_upgrade("player", "detection_risk_add_dodge_chance") then
				player_style = "cj_cloaker"
				myspecial = "cj_cloaker"
			end
			
			-- Inspire Aced + ICTV = Medic Bulldozer
			if managers.player:has_category_upgrade("cooldown", "long_dis_revive") and armor_level >= 7 then
				player_style = "cj_medicdozer"
				myspecial = "cj_medicdozer"
			end
			
		end
		
		if character.data then
			
			if character.data.ai then
				-- Don't do anything with AI
				-- log("NOT STYLING AI "..character.name)
				
			elseif not is_local_peer then
				-- We are another player, let's see what we can do here
				-- log("STYLING REMOTE PLAYER")
				
				local armor_level = tonumber(string.sub(armor_id, 7))
				
				if armor_level <= 1 then
					player_style = "cj_beatcop"
				elseif armor_level == 2 then
					player_style = "cj_tacticalcop"
				elseif armor_level <= 4 then
					player_style = "cj_swat"
				elseif armor_level <= 6 then
					player_style = "cj_swat_heavy"
				elseif armor_level >= 7 then
					player_style = "cj_bulldozer"
				end
				
				-- log("REMOTE PLAYER SPECIAL: "..tostring(character.visual_state.specialunitupgrade))
				
				if character.visual_state.specialunitupgrade then
					player_style = character.visual_state.specialunitupgrade
				end
			end
		else
			log("No Character Data?")
		end
		
		-- log("FINAL STYLE: "..player_style)
	else
		log("No PlayerManager?")
	end
	
	local character_visual_state = {
		is_local_peer = is_local_peer,
		visual_seed = visual_seed,
		player_style = player_style,
		suit_variation = suit_variation,
		glove_id = glove_id,
		mask_id = mask_id,
		armor_id = armor_id,
		armor_skin = armor_skin
	}
	
	if is_local_peer and myspecial then
		character_visual_state.specialunitupgrade = myspecial
	end
	
	local function get_value_string(value)
		return is_local_peer and tostring(value) or "third_" .. tostring(value)
	end
	
	if player_style then
		local unit_name = tweak_data.blackmarket:get_player_style_value(player_style, character_name, get_value_string("unit"))
		
		if unit_name then
			-- log("LOADING "..character_name.." ("..tostring(character)..") "..tostring(unit_name))
			self:safe_load_asset(character, unit_name, "player_style")
		end
	end
	
	CriminalsManager.set_character_visual_state(unit, character_name, character_visual_state)
	
	character.visual_state = {
		is_local_peer = is_local_peer,
		visual_seed = visual_seed,
		player_style = user_player_style,
		suit_variation = suit_variation,
		glove_id = glove_id,
		mask_id = mask_id,
		armor_id = armor_id,
		armor_skin = armor_skin,
		specialunitupgrade = myspecial
	}
end
