
log("COP JOBS - SUCCESSFULLY HOOKED MENUSCENEMANAGER")

function MenuSceneManager:set_character_mask_by_id(mask_id, blueprint, unit, peer_id, character_name)
	
	if unit then self:_delete_character_mask(unit) end
	
	self:_delete_character_mask(self._character_unit)
end

function MenuSceneManager:_set_character_equipment()
	local unit = self._character_unit
	if not alive(unit) then
		return
	end
	local mask_id, mask_blueprint = nil
	local armor_id = "level_1"
	local armor_skin_id, player_style, suit_variation, glove_id = nil
	local rank = 0
	local secondary, primary, deployable = nil
	
	if self._henchmen_player_override then
		log("HENCHMEN OVERRIDE "..tostring(self._henchmen_player_override))
		local loadout = managers.blackmarket:henchman_loadout(self._henchmen_player_override)
		local crafted_mask = managers.blackmarket:get_crafted_category_slot("masks", loadout.mask_slot)
		if crafted_mask then
			mask_id = crafted_mask.mask_id
			mask_blueprint = crafted_mask.blueprint
		end
		player_style = loadout.player_style
		suit_variation = loadout.suit_variation
		glove_id = loadout.glove_id
		local crafted_primary = managers.blackmarket:get_crafted_category_slot("primaries", loadout.primary_slot)
		if crafted_primary then
			primary = crafted_primary
		end
	else
		local equipped_mask = managers.blackmarket:equipped_mask()
		mask_id = equipped_mask and equipped_mask.mask_id
		mask_blueprint = equipped_mask and equipped_mask.blueprint
		armor_id = managers.blackmarket:equipped_armor()
		armor_skin_id = managers.blackmarket:equipped_armor_skin()
		player_style = managers.blackmarket.CurrentlyPreviewing or managers.blackmarket:equipped_player_style()
		suit_variation = managers.blackmarket:get_suit_variation()
		glove_id = managers.blackmarket:equipped_glove_id()
		rank = managers.experience:current_rank()
		secondary = managers.blackmarket:equipped_secondary()
		primary = managers.blackmarket:equipped_primary()
		deployable = managers.blackmarket:equipped_deployable()
	end
	
	if mask_id then
		self:_delete_character_mask(self._character_unit)
	end
	
	local mychar = managers.blackmarket:get_preferred_character()
	
	if not player_style then
		log("WHY IS PLAYER STYLE NON EXISTENT???")
		return
	end
	
	if not tweak_data.blackmarket.player_styles[player_style].cj_style then
		log("REVERTING INVALID STYLE "..player_style)
		player_style = "none"
		managers.blackmarket:CJ_RevertInvalidStyle()
	end
	
	if not tweak_data.blackmarket.player_styles[player_style] then 
		log("CAN'T FIND STYLE "..player_style)
		return
	end
	
	-- Main Outfit
	if tweak_data.blackmarket.player_styles[player_style].characters then
		
		if not tweak_data.blackmarket.player_styles[player_style].characters[mychar] then
			mychar = CriminalsManager.convert_old_to_new_character_workname(mychar)
		end
		
		-- log("------------------------------------------------------------------------------")
		-- log("SETTING "..armor_id.." "..mychar.." "..suit_variation.." "..player_style)
		
		if tweak_data.blackmarket.player_styles[player_style].characters[mychar] then
			
			-- log("OLLUNIT::  "..tostring(tweak_data.blackmarket.player_styles[player_style].characters[mychar].third_unit))
			
			local new_third_unit = tweak_data.blackmarket:GetCJThirdUnitForMenu(armor_id, mychar, player_style, suit_variation)
			
			-- log("NEWUNIT::  "..tostring(new_third_unit))
			
			tweak_data.blackmarket.player_styles[player_style].characters[mychar].third_unit = new_third_unit
			
			-- for s, _ in pairs(tweak_data.blackmarket.player_styles) do
				-- if tweak_data.blackmarket.player_styles[s].police_shared then
					-- log("duplicating unit to ".. s)
					-- tweak_data.blackmarket.player_styles[s].characters[mychar].third_unit = new_third_unit
				-- end
			-- end
		end
	end
	
	-- Alternate Materials
	if tweak_data.blackmarket.player_styles[player_style].mat_overrides then
		
		-- log("OL MAT: "..tostring(tweak_data.blackmarket.player_styles[player_style].material_variations.default.third_material))
		
		local mat = tweak_data.blackmarket:GetCJThirdMatForMenu(armor_id, player_style)
		
		-- log("NEWMAT: "..tostring(mat))
		
		tweak_data.blackmarket.player_styles[player_style].material_variations.default.third_material = mat
		
		-- for s, _ in pairs(tweak_data.blackmarket.player_styles) do
			-- if tweak_data.blackmarket.player_styles[s].police_shared then
				-- log("duplicating material to ".. s)
				-- tweak_data.blackmarket.player_styles[s].material_variations.default.third_material = mat
			-- end
		-- end
	else
		log("NO MAT OVERRIDES FOR STYLE "..player_style)
	end
	
	self:set_character_armor(armor_id, unit)
	self:set_character_armor_skin(armor_skin_id, unit)
	self:set_character_player_style(player_style, suit_variation, unit)
	self:set_character_gloves(glove_id, unit)
	
	local ignore_infamy_card = true --self._scene_templates and self._scene_templates[self._current_scene_template] and self._scene_templates[self._current_scene_template].remove_infamy_card and true or false
	local ignore_weapons = self._scene_templates and self._scene_templates[self._current_scene_template] and self._scene_templates[self._current_scene_template].remove_weapons and true or false
	
	if rank > 0 and not ignore_infamy_card then
		self:set_character_equipped_card(unit, rank - 1)
	elseif secondary and not ignore_weapons then
		self:set_character_equipped_weapon(unit, secondary.factory_id, secondary.blueprint, "secondary", secondary.cosmetics)
	else
		self:_delete_character_weapon(unit, "secondary")
	end
	
	if primary and not ignore_weapons then
		self:set_character_equipped_weapon(unit, primary.factory_id, primary.blueprint, "primary", primary.cosmetics)
	else
		self:_delete_character_weapon(unit, "primary")
	end
	
	self:set_character_deployable(deployable, unit, 0)
end

Hooks:PostHook(MenuSceneManager, "set_henchmen_loadout", "cj_set_henchmen_loadout", function(self, index, character, loadout)
	
	loadout = loadout or managers.blackmarket:henchman_loadout(index)
	character = character or managers.blackmarket:preferred_henchmen(index)
	henchstyle = tostring(loadout.player_style)
	
	-- log("------------------------------------------------------------------------------")
	-- log("SETTING HENCHMEN "..tostring(character).." SLOT "..index.." STYLE "..henchstyle)
	
	if henchstyle == nil or henchstyle == "nil" then
		-- log("WHY IS HENCHEMEN STYLE NON EXISTENT???")
		-- return
		henchstyle = "none"
	end
	
	if not tweak_data.blackmarket.player_styles[henchstyle].cj_style then
		log("REVERTING INVALID STYLE "..henchstyle)
		henchstyle = "none"
	end
	
	if not tweak_data.blackmarket.player_styles[henchstyle] then 
		log("CAN'T FIND STYLE "..henchstyle)
		return
	end
	
	-- Henchmen Outfit
	if tweak_data.blackmarket.player_styles[henchstyle].characters then
		
		local mychar = character
		
		if not tweak_data.blackmarket.player_styles[henchstyle].characters[mychar] then
			mychar = CriminalsManager.convert_old_to_new_character_workname(mychar)
		end
		
		if tweak_data.blackmarket.player_styles[henchstyle].characters[mychar] then
			
			-- log("OLLUNIT::  "..tostring(tweak_data.blackmarket.player_styles[henchstyle].characters[mychar].third_unit))
			
			local new_third_unit = tweak_data.blackmarket:GetCJThirdUnitForMenu("level_1", mychar, henchstyle, nil, true)
			
			-- log("NEWUNIT::  "..tostring(new_third_unit))
			
			tweak_data.blackmarket.player_styles[henchstyle].characters[mychar].third_unit = new_third_unit
			
			-- for s, _ in pairs(tweak_data.blackmarket.player_styles) do
				-- if tweak_data.blackmarket.player_styles[s].police_shared then
					-- log("duplicating unit to ".. s)
					-- tweak_data.blackmarket.player_styles[s].characters[mychar].third_unit = new_third_unit
				-- end
			-- end
		end
	end
	
	-- Henchmen Materials
	if tweak_data.blackmarket.player_styles[henchstyle].mat_overrides then
		
		-- log("OL MAT: "..tostring(tweak_data.blackmarket.player_styles[henchstyle].material_variations.default.third_material))
		
		local mat = tweak_data.blackmarket:GetCJThirdMatForMenu("level_1", henchstyle, true)
		
		-- log("NEWMAT: "..tostring(mat))
		
		tweak_data.blackmarket.player_styles[henchstyle].material_variations.default.third_material = mat
		
		-- for s, _ in pairs(tweak_data.blackmarket.player_styles) do
			-- if tweak_data.blackmarket.player_styles[s].police_shared then
				-- log("duplicating material to ".. s)
				-- tweak_data.blackmarket.player_styles[s].material_variations.default.third_material = mat
			-- end
		-- end
	else
		log("NO MAT OVERRIDES FOR STYLE "..player_style)
	end
	
end)

function MenuSceneManager:set_lobby_character_out_fit(i, outfit_string, rank)
	local outfit = managers.blackmarket:unpack_outfit_from_string(outfit_string)
	local character = outfit.character
	if managers.network:session() then
		if not managers.network:session():peer(i) then return end
		character = managers.network:session():peer(i):character_id()
	end
	self:change_lobby_character(i, character)
	local unit = self._lobby_characters[i]
	
	-- This network shit is too complicated, just make everyone wear normal clothes in the lobby for now
	
	local player_style = "none"
	local armor_id = "level_1"
	
	-- if equipped style == NYPD then
		-- player_style = 
	-- elseif  then
	-- end
	
	for mychar, _ in pairs(tweak_data.blackmarket.player_styles.none.characters_beatcop) do
		
		-- log("OVERRRIDIING LOBBY SETTINGS FOR "..mychar.." STYLE "..tostring(player_style))
		
		-- Main Outfit
		if tweak_data.blackmarket.player_styles[player_style].characters then
			
			if not tweak_data.blackmarket.player_styles[player_style].characters[mychar] then
				mychar = CriminalsManager.convert_old_to_new_character_workname(mychar)
			end
			
			if tweak_data.blackmarket.player_styles[player_style].characters[mychar] then
				
				-- log("OLLUNIT::  "..tostring(tweak_data.blackmarket.player_styles[player_style].characters[mychar].third_unit))
				
				local new_third_unit = tweak_data.blackmarket:GetCJThirdUnitForMenu(armor_id, mychar, player_style, nil, true)
				
				-- log("NEWUNIT::  "..tostring(new_third_unit))
				
				tweak_data.blackmarket.player_styles[player_style].characters[mychar].third_unit = new_third_unit
				
				-- for s, _ in pairs(tweak_data.blackmarket.player_styles) do
					-- if tweak_data.blackmarket.player_styles[s].police_shared then
						-- log("duplicating unit to ".. s)
						-- tweak_data.blackmarket.player_styles[s].characters[mychar].third_unit = new_third_unit
					-- end
				-- end
			end
		end
	end
	
	-- Alternate Materials
	if tweak_data.blackmarket.player_styles[player_style].mat_overrides then
		
		-- log("OL MAT: "..tostring(tweak_data.blackmarket.player_styles[player_style].material_variations.default.third_material))
		
		local mat = tweak_data.blackmarket:GetCJThirdMatForMenu(armor_id, player_style, true)
		
		-- log("NEWMAT: "..tostring(mat))
		
		tweak_data.blackmarket.player_styles[player_style].material_variations.default.third_material = mat
		
		-- for s, _ in pairs(tweak_data.blackmarket.player_styles) do
			-- if tweak_data.blackmarket.player_styles[s].police_shared then
				-- log("duplicating material to ".. s)
				-- tweak_data.blackmarket.player_styles[s].material_variations.default.third_material = mat
			-- end
		-- end
	else
		log("NO MAT OVERRIDES FOR STYLE "..player_style)
	end
	
	
	self:set_character_mask_by_id(outfit.mask.mask_id, outfit.mask.blueprint, unit, i)
	self:set_character_armor("level_1", unit)
	self:set_character_deployable(outfit.deployable, unit, i)
	self:set_character_armor_skin(outfit.armor_skin or managers.blackmarket:equipped_armor_skin(), unit)
	-- self:set_character_player_style(outfit.player_style or managers.blackmarket:equipped_player_style(), outfit.suit_variation or managers.blackmarket:get_suit_variation(), unit)
	self:set_character_player_style(player_style, unit)
	self:set_character_gloves(outfit.glove_id or managers.blackmarket:equipped_glove_id(), unit)
	self:_delete_character_weapon(unit, "all")

	local prio_item = self:_get_lobby_character_prio_item(rank, outfit)

	if prio_item == "rank" then
		self:set_character_card(i, rank, unit)
	else
		self:_select_lobby_character_pose(i, unit, outfit[prio_item])
		self:set_character_equipped_weapon(unit, outfit[prio_item].factory_id, outfit[prio_item].blueprint, "primary", outfit[prio_item].cosmetics)
	end

	local is_me = i == managers.network:session():local_peer():id()
	local mvec = Vector3()
	local math_up = math.UP
	local pos = Vector3()
	local rot = Rotation()

	mrotation.set_yaw_pitch_roll(rot, self._characters_rotation[(is_me and 4 or 0) + i], 0, 0)
	mvector3.set(pos, self._characters_offset)

	if is_me then
		mvector3.set_y(pos, mvector3.y(pos) + 100)
	end

	mvector3.rotate_with(pos, rot)
	mvector3.set(mvec, pos)
	mvector3.negate(mvec)
	mvector3.set_z(mvec, 0)
	mrotation.set_look_at(rot, mvec, math_up)
	unit:set_position(pos)
	unit:set_rotation(rot)
	self:set_lobby_character_visible(i, true)
end

-- Below code is for getting rid of infamy cards
function MenuSceneManager:_get_lobby_character_prio_item(rank, outfit)
	return "primary"
end

function MenuSceneManager:set_scene_template(template, data, custom_name, skip_transition)
	if not skip_transition and (self._current_scene_template == template or self._current_scene_template == custom_name) then
		return
	end

	local template_data = nil

	if not skip_transition then
		managers.menu_component:play_transition()

		self._fov_mod = 0

		if self._camera_object then
			self._camera_object:set_fov(self._current_fov + (self._fov_mod or 0))
		end

		template_data = data or self._scene_templates[template]
		self._current_scene_template = custom_name or template
		self._character_values = self._character_values or {}

		if template_data.character_pos then
			self._character_values.pos_current = self._character_values.pos_current or Vector3()

			mvector3.set(self._character_values.pos_current, template_data.character_pos)
		elseif self._character_values.pos_target then
			self._character_values.pos_current = self._character_values.pos_current or Vector3()

			mvector3.set(self._character_values.pos_current, self._character_values.pos_target)
		end

		local set_character_position = false

		if template_data.character_pos then
			self._character_values.pos_target = self._character_values.pos_target or Vector3()

			mvector3.set(self._character_values.pos_target, template_data.character_pos)

			set_character_position = true
		elseif self._character_values.pos_current then
			self._character_values.pos_target = self._character_values.pos_target or Vector3()

			mvector3.set(self._character_values.pos_target, self._character_values.pos_current)

			set_character_position = true
		end

		if set_character_position and self._character_values.pos_target then
			self._character_unit:set_position(self._character_values.pos_target)
		end

		if _G.IS_VR then
			if template_data.character_rot then
				self._character_unit:set_rotation(template_data.character_rot)
			else
				local a = self._bg_unit:get_object(Idstring("a_reference"))

				self._character_unit:set_rotation(a:rotation())
			end
		end

		if template_data and template_data.recreate_character and self._player_character_name then
			self:set_character(self._player_character_name, true)
		end

		self:_chk_character_visibility(self._character_unit)

		if self._lobby_characters then
			for _, unit in pairs(self._lobby_characters) do
				self:_chk_character_visibility(unit)
			end
		end

		if self._henchmen_characters then
			for _, unit in pairs(self._henchmen_characters) do
				self:_chk_character_visibility(unit)
			end
		end

		self:_use_environment(template_data.environment or "standard")
		self:post_ambience_event(template_data.ambience_event or "menu_main_ambience")

		self._camera_values.camera_pos_current = self._camera_values.camera_pos_target
		self._camera_values.target_pos_current = self._camera_values.target_pos_target
		self._camera_values.fov_current = self._camera_values.fov_target

		if self._transition_time then
			self:dispatch_transition_done()
		end

		self._transition_time = 1
		self._camera_values.camera_pos_target = template_data.camera_pos or self._camera_values.camera_pos_current
		self._camera_values.target_pos_target = template_data.target_pos or self._camera_values.target_pos_current
		self._camera_values.fov_target = template_data.fov or self._standard_fov

		self:_release_item_grab()
		self:_release_character_grab()

		self._use_item_grab = template_data.use_item_grab
		self._use_character_grab = template_data.use_character_grab
		self._use_character_grab2 = template_data.use_character_grab2
		self._use_character_pan = template_data.use_character_pan
		self._disable_rotate = template_data.disable_rotate or false
		self._disable_item_updates = template_data.disable_item_updates or false
		self._can_change_fov = template_data.can_change_fov or false
		self._can_move_item = template_data.can_move_item or false
		self._change_fov_sensitivity = template_data.change_fov_sensitivity or 1
		self._characters_deployable_visible = template_data.characters_deployable_visible or false

		self:set_character_deployable(managers.blackmarket:equipped_deployable(), false, 0)

		if self._card_units and self._card_units[self._character_unit:key()] then
			local secondary = managers.blackmarket:equipped_secondary()

			if secondary then
				self:set_character_equipped_weapon(nil, secondary.factory_id, secondary.blueprint, "secondary", secondary.cosmetics)
			end
		end

		if template_data.hide_weapons then
			self:_delete_character_weapon(self._character_unit, "all")
		end

		-- if template_data.hide_mask then
			self:_delete_character_mask(self._character_unit)
		-- end

		if not _G.IS_VR then
			self:_select_character_pose()
		end

		if alive(self._menu_logo) then
			self._menu_logo:set_visible(not template_data.hide_menu_logo)
		end
	end

	if template_data and template_data.upgrade_object then
		self._temp_upgrade_object = template_data.upgrade_object

		self:_set_item_offset(template_data.upgrade_object:oobb())
	elseif self._use_item_grab and self._item_unit then
		if self._item_unit.unit then
			managers.menu_scene:_set_weapon_upgrades(self._current_weapon_id)
			self:_set_item_offset(self._current_item_oobb_object:oobb())
		else
			self._item_unit.scene_template = {
				template = template,
				data = data,
				custom_name = custom_name
			}
		end
	end

	if not skip_transition then
		local fade_lights = {}

		for _, light in ipairs(self._fade_down_lights) do
			if light:multiplier() ~= 0 and template_data.lights and not table.contains(template_data.lights, light) then
				table.insert(fade_lights, light)
			end
		end

		for _, light in ipairs(self._active_lights) do
			table.insert(fade_lights, light)
		end

		self._fade_down_lights = fade_lights
		self._active_lights = {}

		if template_data.lights then
			for _, light in ipairs(template_data.lights) do
				light:set_enable(true)
				table.insert(self._active_lights, light)
			end
		end
	end

	if template_data then
		if template_data.use_workbench_room then
			self:spawn_workbench_room()
		else
			self:delete_workbench_room()
		end
	end

	managers.network.account:inventory_load()
end
