
log("COP JOBS - SUCCESSFULLY HOOKED PROJECTILESTWEAKDATA")

Hooks:PostHook(BlackMarketTweakData, "_init_projectiles", "cj_init_projectiles", function(self, tweak_data)
	
	local badthrowables = table.list_to_set({
		'frag',
		'frag_com',
		'dada_com',
		'fir_com',
		'dynamite',
		'molotov',
		'wpn_prj_ace',
		'wpn_prj_four',
		'wpn_prj_jav',
		'wpn_prj_target',
		'wpn_prj_hur'
	})
	
	for id, data in pairs(self.projectiles) do
		if badthrowables[id] then
			self.projectiles[id].dlc = "notforpolice"
		end
	end
	
end)
