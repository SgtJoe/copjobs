
dofile(ModPath .. "hooks/cj.lua")
if not RunMod then return end

log("COP JOBS - SUCCESSFULLY HOOKED CHARACTERTWEAKDATA")

local difficulty = Global.game_settings and Global.game_settings.difficulty

Hooks:PostHook(CharacterTweakData, "init", "init_after", function(self, tweak_data)
	
	local gangvox = "none"
	
	if difficulty == "normal" then
		gangvox = "ict"
		
	elseif difficulty == "hard" then
		gangvox = "bik"
		
	elseif difficulty == "overkill" or difficulty == "overkill_145" then
		gangvox = "lt"
		
	elseif difficulty == "easy_wish" or difficulty == "overkill_290" then
		gangvox = "rt"
		self.fbi_heavy_swat.use_radio = "dsp_radio_russian"
		self.medic.use_radio = "dsp_radio_russian"
	end
	
	self.swat.speech_prefix_p1 = gangvox
	self.heavy_swat.speech_prefix_p1 = gangvox
	self.fbi_heavy_swat.speech_prefix_p1 = gangvox
	self.medic.speech_prefix_p1 = gangvox
	
	self.old_hoxton_mission.speech_prefix_p1 = "m"
	
	self.tank.speech_prefix_p1 = "rbdz"
	
	self.shield.die_sound_event = nil
end)

function CharacterTweakData:_init_swat(presets)
	self.swat = deep_clone(presets.base)
	self.swat.tags = {
		"law"
	}
	self.swat.experience = {}
	self.swat.weapon = presets.weapon.good
	self.swat.detection = presets.detection.normal
	self.swat.HEALTH_INIT = 8
	self.swat.headshot_dmg_mul = 2
	self.swat.move_speed = presets.move_speed.fast
	self.swat.suppression = presets.suppression.hard_agg
	self.swat.ecm_vulnerability = 1
	self.swat.ecm_hurts = {
		ears = {
			max_duration = 10,
			min_duration = 8
		}
	}
	self.swat.no_retreat = false
	self.swat.no_arrest = true
	self.swat.weapon_voice = "2"
	self.swat.experience.cable_tie = "tie_swat"
	self.swat.access = "swat"
	self.swat.dodge = presets.dodge.average
	self.swat.rescue_hostages = true
	self.swat.use_radio = nil
	self.swat.calls_in = nil
	self.swat.melee_weapon = "fists"
	self.swat.steal_loot = true
	
	self.swat.surrender = presets.surrender.easy
	
	self.swat.speech_prefix_p2 = nil
	self.swat.speech_prefix_count = 2
	self.swat.chatter = {
		aggressive = true,
		retreat = true,
		contact = true,
		go_go = true,
		suppress = true
	}
	
	table.insert(self._enemy_list, "swat")
end

function CharacterTweakData:_init_heavy_swat(presets)
	self.heavy_swat = deep_clone(presets.base)
	self.heavy_swat.tags = {
		"law"
	}
	self.heavy_swat.experience = {}
	self.heavy_swat.weapon = presets.weapon.good
	self.heavy_swat.detection = presets.detection.normal
	self.heavy_swat.HEALTH_INIT = 10
	self.heavy_swat.headshot_dmg_mul = 2
	self.heavy_swat.damage.explosion_damage_mul = 1
	self.heavy_swat.move_speed = presets.move_speed.fast
	self.heavy_swat.suppression = presets.suppression.hard_agg
	self.heavy_swat.ecm_vulnerability = 1
	self.heavy_swat.ecm_hurts = {
		ears = {
			max_duration = 10,
			min_duration = 8
		}
	}
	self.heavy_swat.weapon_voice = "3"
	self.heavy_swat.experience.cable_tie = "tie_swat"
	self.heavy_swat.access = "swat"
	self.heavy_swat.dodge = presets.dodge.heavy
	self.heavy_swat.no_retreat = false
	self.heavy_swat.no_arrest = true
	self.heavy_swat.rescue_hostages = true
	self.heavy_swat.calls_in = nil
	self.heavy_swat.use_radio = nil
	self.heavy_swat.melee_weapon = "fists"
	self.heavy_swat.steal_loot = true
	
	self.heavy_swat.surrender = presets.surrender.hard
	
	self.heavy_swat.speech_prefix_p2 = nil
	self.heavy_swat.speech_prefix_count = 2
	self.heavy_swat.chatter = {
		aggressive = true,
		retreat = true,
		contact = true,
		go_go = true,
		suppress = true
	}
	
	table.insert(self._enemy_list, "heavy_swat")
end

function CharacterTweakData:_init_fbi_heavy_swat(presets)
	self.fbi_heavy_swat = deep_clone(presets.base)
	self.fbi_heavy_swat.tags = {
		"law"
	}
	self.fbi_heavy_swat.experience = {}
	self.fbi_heavy_swat.weapon = presets.weapon.good
	self.fbi_heavy_swat.detection = presets.detection.guard
	self.fbi_heavy_swat.HEALTH_INIT = 12
	self.fbi_heavy_swat.headshot_dmg_mul = 2
	self.fbi_heavy_swat.damage.explosion_damage_mul = 1
	self.fbi_heavy_swat.move_speed = presets.move_speed.fast
	self.fbi_heavy_swat.suppression = presets.suppression.hard_agg
	self.fbi_heavy_swat.surrender = nil
	self.fbi_heavy_swat.ecm_vulnerability = 1
	self.fbi_heavy_swat.ecm_hurts = {
		ears = {
			max_duration = 10,
			min_duration = 8
		}
	}
	self.fbi_heavy_swat.weapon_voice = "2"
	self.fbi_heavy_swat.experience.cable_tie = "tie_swat"
	self.fbi_heavy_swat.access = "swat"
	self.fbi_heavy_swat.dodge = presets.dodge.athletic
	self.fbi_heavy_swat.no_retreat = false
	self.fbi_heavy_swat.no_arrest = true
	self.fbi_heavy_swat.rescue_hostages = true
	
	self.fbi_heavy_swat.calls_in = nil
	self.fbi_heavy_swat.has_alarm_pager = true
	self.fbi_heavy_swat.radio_prefix = "fri_"
	self.fbi_heavy_swat.use_radio = "dispatch_generic_message"
	
	self.fbi_heavy_swat.melee_weapon = "fists"
	self.fbi_heavy_swat.steal_loot = true
	self.fbi_heavy_swat.silent_priority_shout = "f37"
	
	self.fbi_heavy_swat.speech_prefix_p2 = nil
	self.fbi_heavy_swat.speech_prefix_count = 2
	self.fbi_heavy_swat.chatter = {
		aggressive = true,
		retreat = true,
		contact = true,
		go_go = true,
		suppress = true
	}
	
	table.insert(self._enemy_list, "fbi_heavy_swat")
end

function CharacterTweakData:_init_inside_man(presets)
	self.inside_man = deep_clone(presets.base)
	self.inside_man.tags = {
		"law"
	}
	self.inside_man.experience = {}
	self.inside_man.weapon = presets.weapon.expert
	self.inside_man.detection = presets.detection.normal
	self.inside_man.HEALTH_INIT = 10
	self.inside_man.headshot_dmg_mul = 2
	self.inside_man.damage.explosion_damage_mul = 1
	self.inside_man.move_speed = presets.move_speed.lightning
	self.inside_man.suppression = nil
	self.inside_man.surrender = nil
	self.inside_man.ecm_vulnerability = 1
	self.inside_man.ecm_hurts = {
		ears = {
			max_duration = 10,
			min_duration = 8
		}
	}
	self.inside_man.weapon_voice = "2"
	self.inside_man.experience.cable_tie = "tie_swat"
	self.inside_man.access = "swat"
	self.inside_man.dodge = presets.dodge.ninja
	self.inside_man.dodge_with_grenade = {
		smoke = {
			duration = {
				10,
				20
			}
		},
		check = function (t, nr_grenades_used)
			local delay_till_next_use = math.lerp(17, 45, math.min(1, (nr_grenades_used or 0) / 4))
			local chance = math.lerp(1, 0.5, math.min(1, (nr_grenades_used or 0) / 10))

			if math.random() < chance then
				return true, t + delay_till_next_use
			end

			return false, t + delay_till_next_use
		end
	}
	self.inside_man.no_retreat = true
	self.inside_man.no_arrest = true
	self.inside_man.rescue_hostages = false
	self.inside_man.calls_in = nil
	
	self.inside_man.has_alarm_pager = false
	self.inside_man.use_radio = nil
	
	self.inside_man.melee_weapon = "knife_1"
	self.inside_man.melee_weapon_dmg_multiplier = 10
	self.inside_man.steal_loot = false
	self.inside_man.chatter = presets.enemy_chatter.no_chatter
end

function CharacterTweakData:_init_medic(presets)
	self.medic = deep_clone(presets.base)
	self.medic.tags = {
		"law",
		"medic",
		"special"
	}
	self.medic.experience = {}
	self.medic.weapon = presets.weapon.normal
	self.medic.detection = presets.detection.normal
	self.medic.HEALTH_INIT = 10
	self.medic.headshot_dmg_mul = 2
	self.medic.damage.hurt_severity = presets.hurt_severities.no_hurts
	self.medic.suppression = presets.suppression.no_supress
	self.medic.surrender = presets.surrender.special
	self.medic.move_speed = presets.move_speed.fast
	self.medic.surrender_break_time = {
		7,
		12
	}
	self.medic.ecm_vulnerability = 1
	self.medic.ecm_hurts = {
		ears = {
			max_duration = 10,
			min_duration = 8
		}
	}
	self.medic.weapon_voice = "2"
	self.medic.experience.cable_tie = "tie_swat"
	self.medic.spawn_sound_event = nil
	self.medic.silent_priority_shout = "f37"
	self.medic.access = "swat"
	self.medic.dodge = presets.dodge.heavy
	self.medic.deathguard = true
	self.medic.no_arrest = true
	self.medic.rescue_hostages = true
	
	self.medic.calls_in = nil
	self.medic.use_radio = "fri_dispatch_generic_message"
	
	self.medic.steal_loot = true
	self.medic.priority_shout = "f47"
	self.medic.priority_shout_max_dis = 700
	
	self.medic.speech_prefix_p2 = nil
	self.medic.speech_prefix_count = 2
	self.medic.chatter = {
		aggressive = true,
		retreat = true,
		contact = true,
		go_go = true,
		suppress = true
	}
	
	table.insert(self._enemy_list, "medic")
end

function CharacterTweakData:_init_drug_lord_boss(presets)
	self.drug_lord_boss = deep_clone(presets.base)
	self.drug_lord_boss.tags = {
		"law",
		"tank",
		"special"
	}
	self.drug_lord_boss.experience = {}
	self.drug_lord_boss.weapon = deep_clone(presets.weapon.good)
	self.drug_lord_boss.weapon.is_rifle = {
		aim_delay = {
			0.1,
			0.2
		},
		focus_delay = 4,
		focus_dis = 200,
		spread = 20,
		miss_dis = 40,
		RELOAD_SPEED = 1,
		melee_speed = 1,
		melee_dmg = 25,
		melee_retry_delay = {
			1,
			2
		},
		range = {
			optimal = 2500,
			far = 5000,
			close = 1000
		},
		autofire_rounds = {
			20,
			30
		},
		FALLOFF = {
			{
				dmg_mul = 3,
				r = 100,
				acc = {
					0.6,
					0.9
				},
				recoil = {
					0.4,
					0.7
				},
				mode = {
					0,
					0,
					0,
					1
				}
			},
			{
				dmg_mul = 3,
				r = 500,
				acc = {
					0.5,
					0.7
				},
				recoil = {
					0.4,
					0.7
				},
				mode = {
					0,
					1,
					2,
					8
				}
			},
			{
				dmg_mul = 1.5,
				r = 1000,
				acc = {
					0.4,
					0.6
				},
				recoil = {
					0.45,
					0.8
				},
				mode = {
					1,
					3,
					6,
					6
				}
			},
			{
				dmg_mul = 1,
				r = 2000,
				acc = {
					0.2,
					0.5
				},
				recoil = {
					0.45,
					0.8
				},
				mode = {
					1,
					2,
					2,
					1
				}
			},
			{
				dmg_mul = 1,
				r = 3000,
				acc = {
					0.1,
					0.35
				},
				recoil = {
					1,
					1.2
				},
				mode = {
					4,
					2,
					1,
					0
				}
			}
		}
	}
	self.drug_lord_boss.detection = presets.detection.normal
	self.drug_lord_boss.HEALTH_INIT = 200
	self.drug_lord_boss.headshot_dmg_mul = 2
	self.drug_lord_boss.damage.explosion_damage_mul = 1.1
	self.drug_lord_boss.damage.hurt_severity = presets.hurt_severities.no_hurts
	self.drug_lord_boss.move_speed = presets.move_speed.slow
	self.drug_lord_boss.allowed_stances = {
		cbt = true
	}
	self.drug_lord_boss.allowed_poses = {
		stand = true
	}
	self.drug_lord_boss.crouch_move = false
	self.drug_lord_boss.no_run_start = true
	self.drug_lord_boss.no_run_stop = true
	self.drug_lord_boss.no_retreat = true
	self.drug_lord_boss.no_arrest = true
	self.drug_lord_boss.surrender = nil
	self.drug_lord_boss.ecm_vulnerability = 0.85
	self.drug_lord_boss.ecm_hurts = {
		ears = {
			max_duration = 3,
			min_duration = 1
		}
	}
	self.drug_lord_boss.weapon_voice = "3"
	self.drug_lord_boss.experience.cable_tie = "tie_swat"
	self.drug_lord_boss.access = "tank"
	self.drug_lord_boss.speech_prefix_p1 = "bb"
	self.drug_lord_boss.speech_prefix_p2 = nil
	self.drug_lord_boss.speech_prefix_count = 1
	self.drug_lord_boss.rescue_hostages = false
	self.drug_lord_boss.priority_shout = "f30"
	self.drug_lord_boss.melee_weapon = "fists"
	self.drug_lord_boss.melee_weapon_dmg_multiplier = 2.5
	self.drug_lord_boss.melee_anims = {
		"cbt_std_melee"
	}
	self.drug_lord_boss.steal_loot = nil
	self.drug_lord_boss.calls_in = nil
	self.drug_lord_boss.chatter = presets.enemy_chatter.no_chatter
	self.drug_lord_boss.use_radio = "fri_dispatch_generic_message"
	self.drug_lord_boss.DAMAGE_CLAMP_BULLET = 80
	self.drug_lord_boss.DAMAGE_CLAMP_EXPLOSION = 80
	self.drug_lord_boss.use_animation_on_fire_damage = false
	self.drug_lord_boss.flammable = true
	self.drug_lord_boss.can_be_tased = true
	self.drug_lord_boss.immune_to_knock_down = true
	self.drug_lord_boss.immune_to_concussion = true
	
	table.insert(self._enemy_list, "drug_lord_boss")
end

-- This one is used for allied SWAT-type units
function CharacterTweakData:_init_city_swat(presets)
	self.city_swat = deep_clone(presets.base)
	self.city_swat.experience = {}
	self.city_swat.weapon = presets.weapon.gang_member
	self.city_swat.detection = presets.detection.normal
	self.city_swat.HEALTH_INIT = 20
	self.city_swat.headshot_dmg_mul = 1
	self.city_swat.move_speed = presets.move_speed.very_fast
	self.city_swat.surrender_break_time = {
		6,
		10
	}
	self.city_swat.suppression = presets.suppression.no_supress
	self.city_swat.surrender = false
	self.city_swat.ecm_vulnerability = 1
	self.city_swat.ecm_hurts = {
		ears = {
			max_duration = 10,
			min_duration = 8
		}
	}
	self.city_swat.weapon_voice = "2"
	self.city_swat.experience.cable_tie = "tie_swat"
	self.city_swat.speech_prefix_p1 = self._prefix_data_p1.heavy_swat()
	self.city_swat.speech_prefix_p2 = self._speech_prefix_p2
	self.city_swat.speech_prefix_count = 4
	self.city_swat.access = "teamAI4"
	self.city_swat.dodge = presets.dodge.athletic
	self.city_swat.no_arrest = true
	self.city_swat.chatter = {
		clear = true,
		ready = true,
		contact = true,
		smoke = true,
		go_go = true,
		aggressive = true,
		follow_me = true
	}
	self.city_swat.melee_weapon = "knife_1"
	self.city_swat.melee_weapon_dmg_multiplier = 3
	self.city_swat.steal_loot = false
	self.city_swat.rescue_hostages = false
	self.city_swat.has_alarm_pager = false
	self.city_swat.calls_in = nil
	self.city_swat.use_radio = "dispatch_generic_message"
	
	table.insert(self._enemy_list, "city_swat")
end

-- This one is used for allied basic scripted/map-spawned units
function CharacterTweakData:_init_cop(presets)
	self.cop = deep_clone(presets.base)
	self.cop.experience = {}
	self.cop.weapon = presets.weapon.gang_member
	self.cop.detection = presets.detection.normal
	self.cop.HEALTH_INIT = 10
	self.cop.headshot_dmg_mul = 1
	self.cop.move_speed = presets.move_speed.very_fast
	self.cop.surrender_break_time = {
		6,
		10
	}
	self.cop.suppression = presets.suppression.no_supress
	self.cop.surrender = false
	self.cop.ecm_vulnerability = 1
	self.cop.ecm_hurts = {
		ears = {
			max_duration = 10,
			min_duration = 8
		}
	}
	self.cop.weapon_voice = "2"
	self.cop.experience.cable_tie = "tie_swat"
	self.cop.speech_prefix_p1 = self._prefix_data_p1.swat()
	self.cop.speech_prefix_p2 = "n"
	self.cop.speech_prefix_count = 4
	self.cop.access = "teamAI4"
	self.cop.dodge = presets.dodge.average
	self.cop.no_arrest = true
	self.cop.chatter = {
		contact = true,
		aggressive = true
	}
	self.cop.melee_weapon = "baton"
	self.cop.melee_weapon_dmg_multiplier = 3
	self.cop.steal_loot = false
	self.cop.rescue_hostages = false
	self.cop.has_alarm_pager = false
	self.cop.calls_in = nil
	self.cop.use_radio = "dispatch_generic_message"
	
	table.insert(self._enemy_list, "cop")
	
	self.cop_scared = deep_clone(self.cop)
	self.cop_female = deep_clone(self.cop)
	table.insert(self._enemy_list, "cop_scared")
	table.insert(self._enemy_list, "cop_female")
end

-- Make allied snipers actually worth getting
function CharacterTweakData:_init_sniper(presets)
	self.sniper = deep_clone(presets.base)
	self.sniper.tags = {
		"law",
		"sniper",
		"special"
	}
	self.sniper.experience = {}
	
	self.sniper.weapon = {
		is_rifle = {}
	}
	self.sniper.weapon.is_rifle = {
		aim_delay = {0,0.1},
		focus_delay = 7,
		focus_dis = 200,
		spread = 1,
		miss_dis = 250,
		RELOAD_SPEED = 1.25,
		melee_speed = presets.weapon.normal.is_rifle.melee_speed,
		melee_dmg = presets.weapon.normal.is_rifle.melee_dmg,
		melee_retry_delay = presets.weapon.normal.is_rifle.melee_retry_delay,
		range = {
			optimal = 15000,
			far = 15000,
			close = 15000
		},
		autofire_rounds = presets.weapon.normal.is_rifle.autofire_rounds,
		use_laser = true,
		FALLOFF = {
			{
				dmg_mul = 5,
				r = 700,
				acc = {
					0.7,
					0.95
				},
				recoil = {
					2,
					4
				},
				mode = {
					1,
					0,
					0,
					0
				}
			},
			{
				dmg_mul = 5,
				r = 3500,
				acc = {
					0.7,
					0.95
				},
				recoil = {
					2,
					4
				},
				mode = {
					1,
					0,
					0,
					0
				}
			},
			{
				dmg_mul = 2.5,
				r = 10000,
				acc = {
					0.7,
					0.95
				},
				recoil = {
					2,
					4
				},
				mode = {
					1,
					0,
					0,
					0
				}
			}
		}
	}
	
	self.sniper.detection = presets.detection.sniper
	self.sniper.HEALTH_INIT = 4
	self.sniper.headshot_dmg_mul = 2
	self.sniper.move_speed = presets.move_speed.normal
	self.sniper.shooting_death = false
	self.sniper.no_move_and_shoot = true
	self.sniper.move_and_shoot_cooldown = 1
	self.sniper.suppression = presets.suppression.easy
	self.sniper.ecm_vulnerability = 1
	self.sniper.ecm_hurts = {
		ears = {
			max_duration = 10,
			min_duration = 8
		}
	}
	self.sniper.weapon_voice = "1"
	self.sniper.experience.cable_tie = "tie_swat"
	self.sniper.speech_prefix_p1 = self._prefix_data_p1.swat()
	self.sniper.speech_prefix_p2 = "n"
	self.sniper.speech_prefix_count = 4
	-- self.sniper.priority_shout = "f34"
	self.sniper.access = "sniper"
	self.sniper.no_retreat = true
	self.sniper.no_arrest = true
	self.sniper.chatter = presets.enemy_chatter.cop
	self.sniper.steal_loot = nil
	self.sniper.rescue_hostages = false
	self.sniper.calls_in = nil
	
	table.insert(self._enemy_list, "sniper")
end

-- cj_mansion VIP
function CharacterTweakData:_init_old_hoxton_mission(presets)
	self.old_hoxton_mission = deep_clone(presets.base)
	self.old_hoxton_mission.experience = {}
	self.old_hoxton_mission.weapon = presets.weapon.gang_member
	self.old_hoxton_mission.detection = presets.detection.normal
	self.old_hoxton_mission.HEALTH_INIT = 50
	self.old_hoxton_mission.headshot_dmg_mul = 1
	self.old_hoxton_mission.move_speed = presets.move_speed.slow
	self.old_hoxton_mission.surrender_break_time = {
		6,
		10
	}
	self.old_hoxton_mission.suppression = presets.suppression.no_supress
	self.old_hoxton_mission.surrender = false
	self.old_hoxton_mission.weapon_voice = "1"
	self.old_hoxton_mission.experience.cable_tie = "tie_swat"
	self.old_hoxton_mission.access = "teamAI4"
	self.old_hoxton_mission.dodge = presets.dodge.athletic
	self.old_hoxton_mission.no_arrest = true
	
	self.old_hoxton_mission.speech_prefix_p1 = "m"
	self.old_hoxton_mission.speech_prefix_p2 = "n"
	self.old_hoxton_mission.chatter = {
		retreat = true,
		aggressive = true,
		go_go = true,
		contact = true,
		suppress = true
	}
	
	self.old_hoxton_mission.melee_weapon = "fists"
	self.old_hoxton_mission.melee_weapon_dmg_multiplier = 3
	self.old_hoxton_mission.steal_loot = false
	self.old_hoxton_mission.rescue_hostages = false
	
	self.old_hoxton_mission.has_alarm_pager = false
	self.old_hoxton_mission.calls_in = nil
	self.old_hoxton_mission.use_radio = nil
end

local character_map_original = CharacterTweakData.character_map

function CharacterTweakData:character_map(...)
	
	local char_map = character_map_original(self, ...)
	local cj_map = {}
	
	cj_map = {
		-- Gangsters
		"cj_streetgang_1",
		"cj_streetgang_2",
		"cj_streetgang_3",
		"cj_streetgang_4",
		
		-- Bikers
		"cj_biker_1",
		"cj_biker_2",
		"cj_biker_2_m4",
		"cj_biker_3",
		"cj_biker_4",
		
		-- Cartel
		"cj_cartel_1",
		"cj_cartel_2",
		"cj_cartel_3",
		"cj_cartel_4",
		"cj_cartel_heavy",
		"cj_cartel_medic",
		"cj_cartel_tank",
		
		-- Russians
		"cj_russian_1",
		"cj_russian_2",
		"cj_russian_3",
		"cj_russian_4",
		"cj_russian_heavy",
		"cj_russian_medic",
		"cj_russian_tank_1",
		"cj_russian_tank_2",
		
		-- Friendlies
		"cj_ally_wpd_cop_1",
		"cj_ally_wpd_cop_2",
		"cj_ally_wpd_sniper",
		"cj_ally_wpd_swat_1",
		"cj_ally_wpd_swat_2",
		"cj_ally_fbi_sniper",
		"cj_ally_fbi_heavy",
		"cj_ally_fbi_agent",
		"cj_ally_fbi_mib",
		"cj_ally_fbi_mansion",
		
		-- Other
		"cj_bank_robber",
		"cj_hitman",
		"cj_ally_vip"
	}
	
	for _, unit in pairs(cj_map) do
		table.insert(char_map.basic.list, unit)
	end
	
	return char_map
end
