
log("COP JOBS - SUCCESSFULLY HOOKED WEAPONTWEAKDATA")

Hooks:PostHook(WeaponTweakData, "init", "cj_weapontweakdata_init", function (self, tweak_data)
	local police_weapons = table.list_to_set({
		-- Rifles
		'amcar',
		-- 'scar',
		'new_m4',
		'g36',
		-- Shotguns
		'r870',
		'benelli',
		'saiga',
		-- Pistols
		'glock_17',
		'new_raging_bull',
		'glock_18c',
		'b92fs',
		'colt_1911',
		-- Akimbo Pistols
		'x_1911',
		'x_b92fs',
		'x_g17',
		'x_g18c',
		'x_rage',
		-- SMGs
		'new_mp5',
		'mp9',
		'schakal',
		-- Akimbo SMGs
		'x_mp5',
		'x_mp9',
		'x_schakal'
	})
	
	for id, value in pairs(self) do
		if self[id].custom then
			log("NOT CHANGING CUSTOM WEAPON "..tostring(id))
			return
		end
	end
	
	for id, value in pairs(self) do
		if self[id].autohit and not police_weapons[id] then
			self[id].global_value = "notforpolice"
		end
	end
end)
