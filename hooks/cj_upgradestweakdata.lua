
log("COP JOBS - SUCCESSFULLY HOOKED UPGRADESTWEAKDATA")

Hooks:PostHook(UpgradesTweakData, "init", "cj_upgradestweakdata_init", function(self, tweak_data)
	
	self.definitions.body_armor1 = {
		name_id = "bm_armor_level_3",
		armor_id = "level_3",
		category = "armor"
	}
	self.definitions.body_armor2 = {
		name_id = "bm_armor_level_2",
		armor_id = "level_2",
		category = "armor"
	}
	--[[
	self.definitions.doctor_bag_quantity = {
		description_text_id = "sentry_gun",
		category = "equipment",
		slot = 1,
		equipment_id = "doctor_bag_quantity",
		tree = 4,
		image = "upgrades_sentry",
		image_slice = "upgrades_sentry_slice",
		title_id = "debug_upgrade_new_equipment",
		prio = "high",
		subtitle_id = "debug_sentry_gun",
		name_id = "debug_silent_sentry_gun",
		icon = "equipment_sentry",
		unlock_lvl = 0,
		step = 6
	}
	]]
end)
