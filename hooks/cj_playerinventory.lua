
dofile(ModPath .. "hooks/cj.lua")
if not RunMod then return end

log("COP JOBS - SUCCESSFULLY HOOKED PLAYERINVENTORY")

Hooks:PostHook(PlayerInventory, "set_mask_visibility", "FugMasks", function(self, state)
	
	if alive(self._mask_unit) then
		for _, linked_unit in ipairs(self._mask_unit:children()) do
			linked_unit:unlink()
			World:delete_unit(linked_unit)
		end
		
		self._mask_unit:unlink()
		
		World:delete_unit(self._mask_unit)
	end
	
end)
