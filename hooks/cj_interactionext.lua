
dofile(ModPath .. "hooks/cj.lua")
if not RunMod then return end

log("COP JOBS - SUCCESSFULLY HOOKED INTERACTIONEXT")

Hooks:PostHook(IntimitateInteractionExt, "interact", "intimidate_interact_after", function(self, player)
	
	-- if self.tweak_data == "arrest_suspect" then
	
	if Network:is_server() then
		-- log("SERVER IS CUFFING SUSPECT")
		
		self:remove_interact()
		self:set_active(false, true)
		self._unit:brain():on_intimidated(1334, player)
	else
		-- log("CLIENT IS CUFFING SUSPECT")
		self._unit:brain():on_intimidated(1334, player)
		
		-- UnitNetworkHandler:long_dis_interaction divides amount by 10 so let's just multiply it by 10
		self._unit:network():send_to_host("long_dis_interaction", 69690, player, false)
	end
	
end)
