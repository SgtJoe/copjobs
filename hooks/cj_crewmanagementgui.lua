
log("COP JOBS - SUCCESSFULLY HOOKED CREWMANAGEMENTGUI")

function CrewManagementGui:open_suit_menu(henchman_index)
	local loadout = managers.blackmarket:henchman_loadout(henchman_index)
	local new_node_data = {
		category = "suits"
	}

	self:create_pages(new_node_data, henchman_index, "player_style", loadout.player_style, 3, 3, 1, "bm_menu_player_styles")

	new_node_data[1].mannequin_player_style = loadout.player_style

	-- self:create_pages(new_node_data, henchman_index, "glove", loadout.glove_id, 3, 3, 1, "bm_menu_gloves")

	-- new_node_data[2].mannequin_glove_id = loadout.glove_id
	new_node_data.hide_detection_panel = true
	new_node_data.character_id = managers.menu_scene:get_henchmen_character(henchman_index) or managers.blackmarket:preferred_henchmen(henchman_index)
	new_node_data.custom_callback = {
		trd_equip = callback(self, self, "select_player_style", henchman_index),
		trd_customize = callback(self, self, "open_suit_customize_menu", henchman_index),
		-- hnd_equip = callback(self, self, "select_glove", henchman_index)
	}
	new_node_data.skip_blur = true
	new_node_data.use_bgs = true
	new_node_data.panel_grid_w_mul = 0.6

	managers.environment_controller:set_dof_distance(10, false)
	managers.menu_scene:remove_item()

	new_node_data.topic_id = "bm_menu_player_styles"
	new_node_data.topic_params = {
		weapon_category = managers.localization:text("bm_menu_player_styles")
	}
	new_node_data.back_callback = callback(MenuCallbackHandler, MenuCallbackHandler, "reset_henchmen_player_override")

	managers.menu_scene:set_henchmen_player_override(henchman_index)
	managers.menu:open_node("blackmarket_outfit_node", {
		new_node_data
	})
end

function CrewManagementGui:mouse_wheel_up(x, y) end
function CrewManagementGui:mouse_wheel_down(x, y) end

function CrewManagementGui:next_page() end
function CrewManagementGui:previous_page() end
