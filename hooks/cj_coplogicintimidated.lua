
dofile(ModPath .. "hooks/cj.lua")
if not RunMod then return end

log("COP JOBS - SUCCESSFULLY HOOKED COPLOGICINTIMIDATED")

function CopLogicIntimidated.on_intimidated(data, amount, aggressor_unit)
	local my_data = data.internal_data
	local anim_data = data.unit:anim_data()
	
	local CuffingInProgress = false
	local CuffedByClient = false
	
	-- This is like the ultimate hack but I couldn't figure out a better way 
	if amount == 1334 then
		-- log("CuffingInProgress...")
		CuffingInProgress = true
	elseif amount == 6969 or amount == 1 then
		-- log("CuffedByClient...")
		CuffedByClient = true
		CuffingInProgress = true
	end
	
	
	if anim_data.hands_up then
		-- log("HANDS UP!!")
		
	elseif anim_data.hands_back and not CuffingInProgress then
		-- log("CUFF EM!")
		return
	end
	
	if not my_data.tied then
		my_data.surrender_break_t = data.char_tweak.surrender_break_time and data.t + math.random(data.char_tweak.surrender_break_time[1], data.char_tweak.surrender_break_time[2], math.random())
		local anim, blocks = nil
		
		if anim_data.hands_up then
			anim = "hands_back"
			blocks = {
				heavy_hurt = -1,
				hurt = -1,
				action = -1,
				light_hurt = -1,
				walk = -1
			}
		elseif anim_data.hands_back then
			-- log("SUSPECT CUFFED!")
			anim = "tied"
			blocks = {
				heavy_hurt = -1,
				hurt_sick = -1,
				action = -1,
				light_hurt = -1,
				hurt = -1,
				walk = -1
			}
		else
			anim = "hands_up"
			blocks = {
				heavy_hurt = -1,
				hurt = -1,
				action = -1,
				light_hurt = -1,
				walk = -1
			}
		end
		
		local action_data = {
			clamp_to_graph = true,
			type = "act",
			body_part = 1,
			variant = anim,
			blocks = blocks
		}
		local act_action = data.unit:brain():action_request(action_data)
		
		if data.unit:anim_data().hands_back then
			
			if data.unit:interaction().tweak_data.icon == "equipment_cable_ties" then
				-- log("We already have an interaction")
			else
				-- log("CUFFING ENABLED!")
				data.unit:interaction():set_tweak_data("intimidate")
				data.unit:interaction():set_active(true, true, false)
			end
		end
		
		if data.unit:anim_data().hands_tied then
			CopLogicIntimidated._do_tied(data, aggressor_unit)
		end
	end
end