
log("COP JOBS - SUCCESSFULLY HOOKED GUITWEAKDATA")

Hooks:PostHook(GuiTweakData, "init", "CJ_GUITweakData_init", function(self)
	
	self.crime_net.sidebar = {
		{
			name_id = "menu_cn_shortcuts",
			icon = "sidebar_expand",
			show_name_while_collapsed = false,
			callback = "clbk_toggle_sidebar"
		},
		{
			visible_callback = "clbk_visible_multiplayer",
			btn_macro = "menu_toggle_filters",
			callback = "clbk_crimenet_filters",
			name_id = "menu_cn_filters_sidebar",
			icon = "sidebar_filters"
		},
		{
			item_class = "CrimeNetSidebarSeparator"
		},
		{
			name_id = "menu_cn_premium_buy",
			icon = "sidebar_broker",
			callback = "clbk_contract_broker"
		},
		{
			item_class = "CrimeNetSidebarSeparator"
		},
		{
			name_id = "menu_cn_contact_info",
			icon = "sidebar_codex",
			callback = "clbk_contact_database"
		}
	}
	
	self.crime_net.codex = {
		{
			name_id = "cj_menu_contacts_gov",
			id = "contacts_gov",
			{
				id = "wpd",
				name_id = "cj_contact_wpd",
				{
					image = "ui/affilates/affiliate_wpd",
					desc_id = "cj_contact_wpd_desc_extended",
					post_event = "none"
				}
			},
			-- {
				-- id = "nypd",
				-- name_id = "cj_contact_nypd",
				-- {
					-- image = "ui/affilates/affiliate_nypd",
					-- desc_id = "cj_contact_nypd_desc_extended",
					-- post_event = "none"
				-- }
			-- },
			{
				id = "fbi",
				name_id = "cj_contact_fbi",
				{
					image = "ui/affilates/affiliate_fbi",
					desc_id = "cj_contact_fbi_desc_extended",
					post_event = "none"
				}
			}
		},
		{
			name_id = "cj_menu_contacts_private",
			id = "contacts_corp",
			{
				id = "gensec",
				name_id = "cj_contact_gensec",
				{
					image = "ui/affilates/affiliate_gensec",
					desc_id = "cj_contact_gensec_desc_extended",
					post_event = "none"
				}
			},
			{
				id = "murky",
				name_id = "cj_contact_murkywater",
				{
					image = "ui/affilates/affiliate_murkywater",
					desc_id = "cj_contact_murkywater_desc_extended",
					post_event = "none"
				}
			}
		}
	}
end)
