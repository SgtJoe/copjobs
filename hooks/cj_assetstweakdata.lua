
dofile(ModPath .. "hooks/cj.lua")
if not RunMod then return end

log("COP JOBS - SUCCESSFULLY HOOKED ASSETSTWEAKDATA")

function AssetsTweakData:_init_assets(tweak_data)
	
	self.cj_asset_sniper = {
		name_id = "cj_asset_sniper",
		texture = "ui/mapassets/asset_sniper",
		stages = all_cj_heists,
		visible_if_locked = true,
		unlock_desc_id = "cj_asset_sniper_desc",
		no_mystery = true,
		money_lock = tweak_data:get_value("money_manager", "mission_asset_cost_medium", 10)
	}
	
	self.cj_asset_sniper_fbi = {
		name_id = "cj_asset_sniper_fbi",
		texture = "ui/mapassets/asset_sniper_fbi",
		stages = cj_fbi_stages,
		visible_if_locked = true,
		unlock_desc_id = "cj_asset_sniper_desc",
		no_mystery = true,
		money_lock = tweak_data:get_value("money_manager", "mission_asset_cost_large", 2)
	}
	
	self.cj_asset_medic = {
		name_id = "cj_asset_medic",
		texture = "ui/mapassets/asset_medic",
		stages = all_cj_heists,
		visible_if_locked = true,
		unlock_desc_id = "cj_asset_medic_desc",
		no_mystery = true,
		money_lock = tweak_data:get_value("money_manager", "mission_asset_cost_small", 10)
	}
	
	self.cj_asset_ammo = {
		name_id = "cj_asset_ammo",
		texture = "ui/mapassets/asset_ammo",
		stages = all_cj_heists,
		visible_if_locked = true,
		unlock_desc_id = "cj_asset_ammo_desc",
		no_mystery = true,
		money_lock = tweak_data:get_value("money_manager", "mission_asset_cost_small", 10)
	}
	
	self.cj_asset_backup_wpd = {
		name_id = "cj_asset_backup_wpd",
		texture = "ui/mapassets/asset_backup_wpd",
		stages = all_cj_heists,
		visible_if_locked = true,
		unlock_desc_id = "cj_asset_backup_wpd_desc",
		no_mystery = true,
		money_lock = tweak_data:get_value("money_manager", "mission_asset_cost_medium", 10)
	}
	
	self.cj_asset_backup_fbi = {
		name_id = "cj_asset_backup_fbi",
		texture = "ui/mapassets/asset_backup_fbi",
		stages = cj_fbi_backup_stages,
		visible_if_locked = true,
		unlock_desc_id = "cj_asset_backup_fbi_desc",
		no_mystery = true,
		money_lock = tweak_data:get_value("money_manager", "mission_asset_cost_large", 3)
	}
	
	if Global.level_data and Global.level_data.level_id and Global.level_data.level_id == "cj_mansion" then
		self.cj_asset_backup_fbi.name_id = "cj_asset_backup_fbi_mansion"
		self.cj_asset_backup_fbi.texture = "ui/mapassets/asset_backup_fbi_mansion"
		self.cj_asset_backup_fbi.unlock_desc_id = "cj_asset_backup_fbi_mansion_desc"
	end
	
	self.cj_asset_turret = {
		name_id = "cj_asset_turret",
		texture = "ui/mapassets/asset_turret",
		stages = cj_turret_stages,
		visible_if_locked = true,
		unlock_desc_id = "cj_asset_turret_desc",
		no_mystery = true,
		money_lock = tweak_data:get_value("money_manager", "mission_asset_cost_large", 10)
	}
	
	-- Global remove snipers from maps that don't have them
	for _, stage in pairs(cj_no_sniper_stages) do
		self.cj_asset_sniper.stages = table.removekey(self.cj_asset_sniper.stages, stage)
		self.cj_asset_sniper_fbi.stages = table.removekey(self.cj_asset_sniper_fbi.stages, stage)
	end
	
	-- Remove FBI assets from other maps
	for _, stage in pairs(cj_fbi_stages) do
		self.cj_asset_sniper.stages = table.removekey(self.cj_asset_sniper.stages, stage)
		self.cj_asset_backup_wpd.stages = table.removekey(self.cj_asset_backup_wpd.stages, stage)
	end
end

function table.removekey(intable, badkey)
	
	local newtable = {}
	
	if not intable then log("Bad Input Table!") return newtable end
	if not badkey then log("Bad Input Data!") return intable end
	
	for id, data in pairs(intable) do
		if data == badkey then
			-- log("REMOVING: "..badkey.." ID "..id)
		else
			table.insert(newtable, data)
		end
	end
	
	return newtable
end

function AssetsTweakData:_init_risk_assets(tweak_data)
	self.risk_pd = {
		name_id = "menu_asset_risklevel_0",
		texture = "ui/mugshots/mugshot_streetgang",
		stages = "all",
		risk_lock = 0
	}
	self.risk_swat = {
		name_id = "menu_asset_risklevel_1",
		texture = "ui/mugshots/mugshot_bikers",
		stages = "all",
		risk_lock = 1
	}
	self.risk_fbi = {
		name_id = "menu_asset_risklevel_2",
		texture = "ui/mugshots/mugshot_cartel",
		stages = "all",
		risk_lock = 2
	}
	self.risk_death_squad = {
		name_id = "menu_asset_risklevel_3",
		texture = "ui/mugshots/mugshot_cartel",
		stages = "all",
		risk_lock = 3
	}
	self.risk_easy_wish = {
		name_id = "menu_asset_risklevel_4",
		texture = "ui/mugshots/mugshot_russians",
		stages = "all",
		risk_lock = 4
	}
	self.risk_death_wish = {
		name_id = "menu_asset_risklevel_5",
		texture = "ui/mugshots/mugshot_russians",
		stages = "all",
		risk_lock = 5
	}
	self.risk_sm_wish = {
		name_id = "menu_asset_risklevel_6",
		texture = "guis/textures/pd2/mission_briefing/assets/assets_risklevel_6",
		stages = "all",
		risk_lock = 6
	}
end

function AssetsTweakData:_init_gage_assets(tweak_data)
	self.gage_assignment = {
		name_id = "menu_asset_gage_assignment",
		texture = "guis/dlcs/gage_pack_jobs/textures/pd2/mission_briefing/assets/gage_assignment",
		stages = nil
	}
end
